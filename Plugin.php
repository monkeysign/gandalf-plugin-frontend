<?php
namespace Plugins\Frontend;

class Plugin extends \Ring\Plugin\Pluggable {

	static function register(){
		parent::register();
		require __DIR__ .'/hooks.php';
	}

}