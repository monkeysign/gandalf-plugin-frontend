<?php

namespace Plugins\Frontend\Controllers;

use mysql_xdevapi\Table;
use Plugins\CRM\Customer\Models\Customer;
use Plugins\CRM\Customer\Models\CustomerAddress;
use Plugins\ECOMMERCE\Models\Brand;
use Plugins\ECOMMERCE\Models\Category;
use Plugins\ECOMMERCE\Models\Feature;
use Plugins\ECOMMERCE\Models\Order;
use Plugins\ECOMMERCE\Models\OrderAddress;
use Plugins\ECOMMERCE\Models\OrderProduct;
use Plugins\ECOMMERCE\Models\Product;
use Plugins\ECOMMERCE\Models\ProductType;
use Plugins\Frontend\Classes\Controller;
use Plugins\CMS\Models\Post;
use Plugins\CRM\Customer\Models\CustomerMeta as CustomerMeta;


define("DEBUG", 1);
define("USE_SANDBOX", 0);

class Index extends Controller
{


    public function __construct(\Illuminate\Http\Request $request)
    {
        parent::__construct($request);
        parent::initTemplate();
    }


    /**
     * La nostra homepage
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function frontpage()
    {


        //Prendo la pagina con permalink = '/'
        $post = \Plugins\CMS\Models\Post::where('id_post_type', 1)->where('permalink', '/')->where('state', 1)->first();

        //Prendo lo slider di immagini
        $gallery = \Plugins\CMS\Models\Post::where('id_post_type', 3)->where('state', 1)->get();

        //Prendo lo hero
        $hero = \Plugins\CMS\Models\Post::where('id_post_type', 4)->where('state', 1)->orderBy('created_at', 'desc')->first();

        //Prendo i banner
        $bannerHome = \Plugins\CMS\Models\Post::where('id_post_type', 6)->where('state', 1)->orderBy('created_at', 'desc')->first();

        // metto nel container per utilizzare le API del tema
        container()->set('the_post', $post);

        $productBestSeller = Product::where('status', 1)->where('quantity', '>', 0)->where('best_seller', 1)->get();
        $productOffer = Product::where('status', 1)->where('quantity', '>', 0)->where('offer', 1)->offset(0)->limit(5)->inRandomOrder()->get();
        $productPicked = Product::where('status', 1)->where('quantity', '>', 0)->where('picked', 1)->orderBy('updated_at', 'DESC')->limit(5)->get();

        // BOX OFFERTE
        /*$categorieProdotti = Category::where('status', 1)->where('level', 0)->orderBy('orders', 'asc')->get();
        $categorieProdottiMiddle = Category::where('status', 1)->where('level', 1)->get();
        $categorieProdottiDown = Category::where('status', 1)->where('level', 2)->get();

        $catMiddle = [];
        foreach ($categorieProdottiMiddle as $cMiddle) {
          $catMiddle[$cMiddle->id] = $cMiddle->id_parent;
        }

        $arrayToFilter = [];
        $prodottiBox1[][] = null;
        $prodottiBoxOfferta[][] = null;
        if ($categorieProdottiDown) {
          foreach ($categorieProdottiDown as $categoria) {
            $arrayToFilter[$catMiddle[$categoria->id_parent]][] = $categoria->id;
          }
        }

        if ($categorieProdotti) {
          foreach ($categorieProdotti as $categoria) {
            if (isset($arrayToFilter[$categoria->id])) {
              $prodottiBox1[$categoria->id] = Product::select('ecommerce_product.*')->where('status', 1)->whereIn('id_category', $arrayToFilter[$categoria->id])->offset(0)->limit(5)->get();
              $prodottiBoxNovita[$categoria->id] = Product::select('ecommerce_product.*')->where('status', 1)->where('id_category', $arrayToFilter[$categoria->id])->where('novelty', 1)->offset(0)->limit(8)->get();
            }
          }
        }


        $prodottiApple[] = null;
        $prodottiApple[] = Product::where('status', 1)->whereIn('id_category', $arrayToFilter[10])->offset(0)->limit(8)->inRandomOrder()->get();*/

        $brand = Brand::where('status', 1)->orderBy('order', 'DESC')->get();

        $this->param['post'] = $post;
//    $this->param['category'] = $categorieProdotti;
//    $this->param['prodottiBox1'] = $prodottiBox1;
        $this->param['productOffer'] = $productOffer;
//    $this->param['productNovelty'] = $prodottiBoxNovita;
        $this->param['brands'] = $brand;
        $this->param['picked'] = $productPicked;
        $this->param['bestSeller'] = $productBestSeller;
        $this->param['hero'] = $hero;
//    $this->param['productApple'] = $prodottiApple;
        $this->param['gallery'] = $gallery;

        $this->param['bannerHome'] = $bannerHome;


        if ($post && $post->meta('seo')) {
            $seo = unserialize($post->meta('seo'));
            if ($seo['title']) {
                $this->param['titleFacebook'] = $seo['title'] . " - Blob Video";
            }
            if ($seo['description']) {
                $description = trim(strip_tags($seo['description']));
                if (strlen($description) > 300)
                    $description = substr($description, 0, 300);
                $this->param['descriptionFacebook'] = $description;
            }
        }
        if ($post) {
            if ($post->meta("imgsocial")) {
                $this->param['imgFacebook'] = config('httpmedia') . '/' . basename($post->meta('imgsocial'));
            }
        }


        view()->render('home', $this->param);
    }

    public function ajaxSearch()
    {
        $URIquery = request()->query;
        $term = $URIquery->get('s');
        $tipologie = $URIquery->get('tipoligie');
        $product = Product::join('ecommerce_category', 'ecommerce_product.id_category', '=', 'ecommerce_category.id')
            ->select('ecommerce_product.*', 'ecommerce_category.search')
            ->where('ecommerce_product.title', 'like', "%{$term}%")
            ->where('ecommerce_product.status', 1)
            ->where('quantity', '>', 0);

        if ($tipologie) {
            $arrayTipologie = explode(",", $tipologie);
            $product->whereIn('ecommerce_category.id_product_type', $arrayTipologie);
        }


        $product = $product->limit(5)
            ->offset(0)
            //->orderBy('ecommerce_category.search', 'desc')
            ->orderBy('ecommerce_product.rilevance', 'DESC')
            ->get();

        //$category = Category::where('title', 'like', "%{$term}%")->get();
        //$category = array();
        $realP = [];

        foreach ($product as $p) {
            $p->image = config('httpmedia') . 'ecommerce/prodotti/' . basename($p->meta('imghighlight'));
            $p->permalink = $p->getPermalink();
            $realP[] = $p;
        }

        return json_encode(['products' => $realP, 'category' => array(), 's' => $URIquery->get('s')]);
    }

    /**
     * Pagina della lista shop
     */
    public function shopList($category = null)
    {
        $this->param['title'] = "I nostri prodotti - Blob Video";
        $subC = array();
        $features = [];
        $attributes = [];
        $URIquery = request()->query;

        // Limit
        if (!$URIquery->get('l')) {
            $URIquery->set('l', 15);
            $limit = 15;
        } else {
            $limit = $URIquery->get('l');
        }

        // Page
        if (!$URIquery->get('p')) {
            $URIquery->set('p', 1);
            $offset = 0;
            $page = 1;
        } else {
            $page = $URIquery->get('p');
            $offset = $limit * ($page - 1);
        }

        // order
        if (!$URIquery->get('ord')) {
            $order = array('ecommerce_product.id', 'desc');
            $URIquery->set('ord', 'id|desc');
        } else {
            $order = explode('|', $URIquery->get('ord'));
            $order[0] = 'ecommerce_product.' . $order[0];
        }


        // preparo le query max price e tutti i record
        $products = Product::
        leftJoin('ecommerce_category', 'ecommerce_product.id_category', '=', 'ecommerce_category.id')
            ->leftJoin('ecommerce_product_category', 'ecommerce_product_category.id_product', '=', 'ecommerce_product.id')
            ->select('ecommerce_product.*');
        $maxAndMin = db()::table('ecommerce_product')
            ->select(db()::raw('MAX(price) as max_price, MIN(price) as min_price, status'))
            ->where('status', 1);

        //offer
        if ($URIquery->get('off') && count($URIquery->get('off')) > 0) {
            $products = $products->where('offer', 1);
        }

        // Category
        $breadcrumb = array();
        $breadcrumbPermalink = array();
        if ($category) {
            /*
            * Esempio permalink categoria
            * http://localhost/blobvideo/playstation-4/giochi-per-ps4
            * */
            $filterCat = array();
            $permalink = $category;
            $category = Category::where('permalink', '=', $category)->first();
            //se non trovo nessun permalink su quella categoria allora verifico che non si tratti di un prodotto
            if (!$category) {
                $splitPerma = explode("/", $permalink);
                $permalinkProdotto = $splitPerma[count($splitPerma) - 1];
                $product = Product::where('permalink', '=', $permalinkProdotto)->first();
                if ($product) {
                    return $this->product($product->permalink);
                }
            }

            /*
             * Esempio permalink prodotto categoriaRoot/prodottoPermalink
             * iphone/cuffie-solo3-wireless-beats-special-edition-oro-rosa
             * */

            $filterCat[] = $category->id;

            $this->param['title'] = "{$category->title} - Blob Video";

            //breadcrumb
            $stepLabel = explode(' | ', $category->path_label);
            $stepId = explode(' | ', $category->path);
            for ($i = 0; $i < count($stepId); $i++) {
                $breadcrumb[$stepId[$i]] = $stepLabel[$i];
                $BreadCategory = Category::where('id', $stepId[$i])->first();
                $breadcrumbPermalink[$stepId[$i]] = $BreadCategory->permalink;

            }

            /*if ($category->productType) {
              foreach ($category->productType->features as $singleF) {
                $features[$singleF->title] = $singleF;
              }
            }*/
            $subC = Category::where('id_parent', $category->id)->orderBy('orders', 'ASC')->get();
            foreach ($subC as $item) {
                $filterCat[] = $item->id;
                /*if ($item->productType && $item->productType->features) {
                  foreach ($item->productType->features as $feature) {
                    $features[$feature->title] = $feature;
                  }
                }*/
            }

            if ($category->level == 0)
                $catToFilter = Category::where('path', 'like', $category->id . ' | %')->get();
            else
                $catToFilter = Category::where('path', 'like', '% | ' . $category->id . ' | %')->get();

            $allSubTree[] = $category->id;
            foreach ($catToFilter as $catFF) {
                $allSubTree[] = $catFF->id;
            }

            //$products = $products->whereIn('ecommerce_product.id_category', $allSubTree);
            $products = $products->where(function ($products) use ($allSubTree) {
                $products
                    ->whereIn('ecommerce_product.id_category', $allSubTree)
                    ->orWhereIn('ecommerce_product_category.id_category', $allSubTree);
            });

            $maxAndMin->whereIn('ecommerce_product.id_category', $allSubTree);


            if ($category->meta("title")) {
                $this->param['title'] = $category->meta("title") . " - Blob Video";
                $this->param['titleFacebook'] = $category->meta("title") . " - Blob Video";
            } else {
                $this->param['title'] = $category->title . " - Blob Video";
                $this->param['titleFacebook'] = $category->title . " - Blob Video";
            }

            if ($category->meta("description")) {
                $description = trim(strip_tags($category->meta("description")));
                if (strlen($description) > 300)
                    $description = substr($description, 0, 300);
                $this->param['description'] = $description;
                $this->param['descriptionFacebook'] = $description;
            }
            if ($category->meta("imgsocial")) {
                $this->param['imgFacebook'] = config('httpmedia') . 'ecommerce/categorie/' . basename($category->meta('imgsocial'));
            } else {
                if ($category->meta('imghighlight'))
                    $this->param['imgFacebook'] = config('httpmedia') . 'ecommerce/categorie/' . basename($category->meta('imghighlight'));
            }

        }

        // Eventuali filtri
        // if(...) $product->where()->where()

        if ($URIquery->get('startp') || $URIquery->get('endp')) {
            $products = $products
                ->where('ecommerce_product.price', '>=', $URIquery->get('startp'))
                ->where('ecommerce_product.price', '<=', $URIquery->get('endp'));
            /*$maxAndMin = $maxAndMin
                ->where('price', '>=', $URIquery->get('startp'))
                ->where('price', '<=', $URIquery->get('endp'));*/
        }

        //Stato Prodotto Selected
        $stateSelected = [];
        if ($URIquery->get('state_product') && count($URIquery->get('state_product')) > 0) {
            $products = $products->whereIn('state_product', array_keys($URIquery->get('state_product')));
            $stateSelected = $URIquery->get('state_product');
        }

        // attributes
        $attrSelected = array();
        if ($URIquery->get('attr') && count($URIquery->get('attr')) > 0) {
            $products = $products
                ->join('ecommerce_product_attribute', function ($join) use ($URIquery) {
                    $join->on('ecommerce_product.id', '=', 'ecommerce_product_attribute.id_product');
                });
            $products = $products->whereIn('ecommerce_product_attribute.id_attribute', array_keys($URIquery->get('attr')));
            $products = $products->groupBy('ecommerce_product.id');
            $attrSelected = $URIquery->get('attr');
        }

        // TODO: attributi validi e marchi filtrati in base ai risultati
        $allBrandId = [];
        $allStateProduct = [];
        $allProductTypeId = [];
        $allFeatures = [];
        $allAttributes = [];

        $noLimitsProduct = $products
            ->where('ecommerce_product.status', 1)
            ->where('ecommerce_product.quantity', '>', 0)
            ->distinct()
            ->get();
        foreach ($noLimitsProduct as $productNoL) {
            // Brand
            $allBrandId[] = $productNoL->id_brand;

            // Stato Prodotto
            switch ($productNoL->state_product) {
                case 0:
                    $allStateProduct['Nuovo'] = 0;
                    break;
                case 1:
                    $allStateProduct['Usato'] = 1;
                    break;
                case 2:
                    $allStateProduct['In Arrivo'] = 2;
                    break;
                case 3:
                    $allStateProduct['In Rientro'] = 3;
                    break;
            }

            // Tipologia
            $allProductTypeId[] = $productNoL->category->id_product_type;

            // Features + Attributi
            $attributesP = $productNoL->attributes;

            if (count($attributesP) > 0) {
                foreach ($attributesP as $sAttr) {
                    $singleF = $sAttr->feature;
                    $features[$singleF->title] = $singleF;
                    $attributes[$sAttr->id_feature][$sAttr->id] = $sAttr;
                }
            }
        }

        // Brand filtrati
        $allBrand = Brand::whereIn('id', $allBrandId)->get();
        $brandSelected = array();
        if ($URIquery->get('brands') && count($URIquery->get('brands')) > 0) {
            $products = $products->whereIn('id_brand', array_keys($URIquery->get('brands')));
            $brandSelected = $URIquery->get('brands');
        }

        // Tipologie Filtrate
        $allProductType = ProductType::whereIn('id', $allProductTypeId)->get();
        $productTypeSelected = array();
        if ($URIquery->get('product_type') && count($URIquery->get('product_type')) > 0) {
            $products = $products->whereIn('ecommerce_category.id_product_type', array_keys($URIquery->get('product_type')));
            $productTypeSelected = $URIquery->get('product_type');
        }


        // prezzo min and max
        $maxAndMin = $maxAndMin->groupBy('status')->first();

        // conta della query
        $countQ = $products
            ->distinct()
            ->get();
        $countP = count($countQ);

        // risultati
        $resProd = $products
            ->orderBy('ecommerce_product.rilevance', 'DESC')
            ->orderBy($order[0], $order[1])
            ->limit($limit)
            ->offset($offset)
            ->distinct()
//      ->toSql();
            ->get();

//    var_dump($resProd);
        $totPage = ceil(($countP / $limit));
        $URIquery->set('totPage', $totPage);
        $URIquery->set('count', $countP);

        $this->param['filterPrices'] = $maxAndMin;
        $this->param['category'] = $category;
        $this->param['subC'] = $subC;
        $this->param['features'] = $features;
        $this->param['attributes'] = $attributes;
        $this->param['products'] = $resProd;
        $this->param['allBrands'] = $allBrand;
        $this->param['allStateProduct'] = $allStateProduct;
        $this->param['allProductType'] = $allProductType;
        $this->param['selectedBrand'] = $brandSelected;
        $this->param['selectedAttr'] = array();
        $this->param['productTypeSelected'] = $productTypeSelected;
        $this->param['query'] = $URIquery;
        $this->param['attrSelected'] = $attrSelected;
        $this->param['stateSelected'] = $stateSelected;
        $this->param['breadcrumb'] = $breadcrumb;
        $this->param['breadcrumbPermalink'] = $breadcrumbPermalink;
        view()->render('shop', $this->param);
    }

    /**
     * Pagina della lista shop
     */
    public function shopOffer($category = null)
    {

//    $this->param['title'] = "Offerte Natalizie  - Blob Video";
//    $this->param['titleFacebook'] = "Offerte Natalizie  - Blob Video";
//    $this->param['description'] = "Più di 100 prodotti in Offerta per le feste: Scoprili tutti!";
//    $this->param['descriptionFacebook'] = "Più di 100 prodotti in Offerta per le feste: Scoprili tutti!";
        $this->param['title'] = "Offerte - Blob Video";
        $this->param['titleFacebook'] = "Offerte - Blob Video";
        $this->param['description'] = "Scopri tutte le nostre fantastiche offerte su blobvideo.com";
        $this->param['descriptionFacebook'] = "Scopri tutte le nostre fantastiche offerte su blobvideo.com";
        $this->param['imgFacebook'] = 'https:' . asset('assets/images/') . "icons/offerte-facebook.png";
        //Prendo i banner
        $bannerHome = \Plugins\CMS\Models\Post::where('id_post_type', 6)->where('state', 1)->orderBy('created_at', 'desc')->first();

        $subC = array();
        $URIquery = request()->query;

        // Limit
        if (!$URIquery->get('l')) {
            $URIquery->set('l', 15);
            $limit = 15;
        } else {
            $limit = $URIquery->get('l');
        }

        // Page
        if (!$URIquery->get('p')) {
            $URIquery->set('p', 1);
            $offset = 0;
            $page = 1;
        } else {
            $page = $URIquery->get('p');
            $offset = $limit * ($page - 1);
        }

        // order
        if (!$URIquery->get('ord')) {
            $order = array('ecommerce_product.id', 'desc');
            $URIquery->set('ord', 'id|desc');
        } else {
            $order = explode('|', $URIquery->get('ord'));
            $order[0] = 'ecommerce_product.' . $order[0];
        }


        // preparo le query max price e tutti i record
        $products = Product::join('ecommerce_category', 'ecommerce_product.id_category', '=', 'ecommerce_category.id')
            ->select('ecommerce_product.*')
            ->where('ecommerce_product.status', 1)
            // non voglio i prodotti in arrivo
            ->where('ecommerce_product.state_product', '<>', 2)
            ->where('ecommerce_product.quantity', '>', 0)
            ->where('offer', 1);

        // TODO: attributi validi e marchi filtrati in base ai risultati
        $allBrandId = [];
        $allStateProduct = [];
        $allProductTypeId = [];

        $noLimitsProduct = $products->where('ecommerce_product.quantity', '>', 0)->distinct()->get();
        foreach ($noLimitsProduct as $productNoL) {
            // Brand
            $allBrandId[] = $productNoL->id_brand;

            // Stato Prodotto
            switch ($productNoL->state_product) {
                case 0:
                    $allStateProduct['Nuovo'] = 0;
                    break;
                case 1:
                    $allStateProduct['Usato'] = 1;
                    break;
                case 2:
                    $allStateProduct['In Arrivo'] = 2;
                    break;
                case 3:
                    $allStateProduct['In Rientro'] = 3;
                    break;
            }

            // Tipologia
            $allProductTypeId[] = $productNoL->category->id_product_type;
        }


        // Tipologie Filtrate
        $allProductType = ProductType::whereIn('id', $allProductTypeId)->get();
        $productTypeSelected = array();
        if ($URIquery->get('product_type') && count($URIquery->get('product_type')) > 0) {
            $products = $products->whereIn('ecommerce_category.id_product_type', array_keys($URIquery->get('product_type')));
            $productTypeSelected = $URIquery->get('product_type');
        }

        // conta della query
        $countQ = $products->where('ecommerce_product.quantity', '>', 0)->distinct()->get();
        $countP = count($countQ);

        // risultati
        $resProd = $products->orderBy('ecommerce_product.rilevance', 'DESC')->where('ecommerce_product.quantity', '>', 0)->distinct()->orderBy($order[0], $order[1])->limit($limit)->offset($offset)->get();

        $totPage = ceil(($countP / $limit));
        $URIquery->set('totPage', $totPage);
        $URIquery->set('count', $countP);

        $this->param['category'] = $category;
        $this->param['subC'] = $subC;
        $this->param['products'] = $resProd;
        $this->param['allProductType'] = $allProductType;
        $this->param['productTypeSelected'] = $productTypeSelected;
        $this->param['query'] = $URIquery;
        $this->param['breadcrumb'] = array();
        $this->param['bannerHome'] = $bannerHome;
        view()->render('shop-offer', $this->param);
    }


    public function shopListSearch()
    {

        $this->param['title'] = "Ricerca - Blob Video";

        $subC = array();
        $features = array();
        $attributes = array();
        $URIquery = request()->query;

        // Limit
        if (!$URIquery->get('l')) {
            $URIquery->set('l', 15);
            $limit = 15;
        } else {
            $limit = $URIquery->get('l');
        }

        // Page
        if (!$URIquery->get('p')) {
            $URIquery->set('p', 1);
            $offset = 0;
            $page = 1;
        } else {
            $page = $URIquery->get('p');
            $offset = $limit * ($page - 1);
        }

        // order
        if (!$URIquery->get('ord')) {
            $order = array('ecommerce_product.id', 'desc');
            $URIquery->set('ord', 'id|desc');
        } else {
            $order = explode('|', $URIquery->get('ord'));
            $order[0] = 'ecommerce_product.' . $order[0];
        }

        $URIquery = request()->query;

        $term = $URIquery->get('term');
        if ($URIquery->get('typology') && count($URIquery->get('typology')) > 0) {
            $tipologie = $URIquery->get('typology');
        } else {
            $tipologie = null;
        }
        $products = Product::join('ecommerce_category', 'ecommerce_product.id_category', '=', 'ecommerce_category.id')
            ->select('ecommerce_product.*', 'ecommerce_category.search')
            ->where('ecommerce_product.title', 'like', "%{$term}%")
            ->where('ecommerce_product.status', 1)
            ->where('quantity', '>', 0);

        //Stato Prodotto Selected
        $stateSelected = [];
        if ($URIquery->get('state_product') && count($URIquery->get('state_product')) > 0) {
            $tipologie = null;
            $products = $products->whereIn('state_product', array_keys($URIquery->get('state_product')));
            $stateSelected = $URIquery->get('state_product');
        }

        // attributes
        $attrSelected = array();
        if ($URIquery->get('attr') && count($URIquery->get('attr')) > 0) {
            $products = $products
                ->join('ecommerce_product_attribute', function ($join) use ($URIquery) {
                    $join->on('ecommerce_product.id', '=', 'ecommerce_product_attribute.id_product');
                });
            $products->whereIn('id_attribute', array_keys($URIquery->get('attr')));
            $products->groupBy('ecommerce_product.id');
            $attrSelected = $URIquery->get('attr');
        }

        // TODO: attributi validi e marchi filtrati in base ai risultati
        $allBrandId = [];
        $allStateProduct = [];
        $allProductTypeId = [];
        $allFeatures = [];
        $allAttributes = [];


        $noLimitsProduct = $products->where('ecommerce_product.quantity', '>', 0)->distinct()->get();
        foreach ($noLimitsProduct as $productNoL) {
            // Brand
            $allBrandId[] = $productNoL->id_brand;

            // Stato Prodotto
            switch ($productNoL->state_product) {
                case 0:
                    $allStateProduct['Nuovo'] = 0;
                    break;
                case 1:
                    $allStateProduct['Usato'] = 1;
                    break;
                case 2:
                    $allStateProduct['In Arrivo'] = 2;
                    break;
                case 3:
                    $allStateProduct['In Rientro'] = 3;
                    break;
            }

            // Tipologia
            $allProductTypeId[] = $productNoL->category->id_product_type;

            // Features + Attributi
            $attributesP = $productNoL->attributes;
            if (count($attributesP) > 0) {
                foreach ($attributesP as $sAttr) {
                    $singleF = $sAttr->feature;
                    $features[$singleF->title] = $singleF;
                    $attributes[$sAttr->id_feature][$sAttr->id] = $sAttr;
                }
            }
        }

        // Brand filtrati
        $allBrand = Brand::whereIn('id', $allBrandId)->get();
        $brandSelected = array();
        if ($URIquery->get('brands') && count($URIquery->get('brands')) > 0) {
            $products = $products->whereIn('id_brand', array_keys($URIquery->get('brands')));
            $brandSelected = $URIquery->get('brands');
        }

        // Tipologie Filtrate
        $allProductType = ProductType::whereIn('id', $allProductTypeId)->get();
        $productTypeSelected = array();
        if ($URIquery->get('product_type') && count($URIquery->get('product_type')) > 0) {
            $products = $products->whereIn('ecommerce_category.id_product_type', array_keys($URIquery->get('product_type')));
            $productTypeSelected = $URIquery->get('product_type');

        }


        //offer
        if ($URIquery->get('off') && count($URIquery->get('off')) > 0) {
            $products = $products->where('offer', 1);
        }

        // Category
        $breadcrumb = array();

        if ($tipologie) {
            $arrayTipologie = explode(",", $tipologie);
            $products->whereIn('ecommerce_category.id_product_type', $arrayTipologie);

            $subC = Category::where('ecommerce_category.level', 1)->whereIn('ecommerce_category.id_product_type', $arrayTipologie)->get();

        } else {
            $subC = Category::where('ecommerce_category.level', 0)->get();
        }


        foreach ($subC as $item) {
            $filterCat[] = $item->id;
        }

        $featuresList = Feature::whereIn('id', array(92, 150))->get();
        foreach ($featuresList as $feature) {
            $features[$feature->title] = $feature;
        }


        // Eventuali filtri
        // if(...) $product->where()->where()
        if ($URIquery->get('startp') || $URIquery->get('endp')) {
            $products = $products
                ->where('price', '>=', $URIquery->get('startp'))
                ->where('price', '<=', $URIquery->get('endp'));
        }


        // conta della query
        $countQ = $products->where('ecommerce_product.quantity', '>', 0)->distinct()->get();
        $countP = count($countQ);

        // risultati
        $resProd = $products->where('ecommerce_product.quantity', '>', 0)->distinct()->orderBy('ecommerce_product.rilevance', 'DESC')->orderBy($order[0], $order[1])->limit($limit)->offset($offset)->get();

        $totPage = ceil(($countP / $limit));
        $URIquery->set('totPage', $totPage);
        $URIquery->set('count', $countP);

        $this->param['subC'] = $subC;
        $this->param['term'] = $term;
        $this->param['features'] = $features;
        $this->param['attributes'] = $attributes;
        $this->param['products'] = $resProd;
        $this->param['allBrands'] = $allBrand;
        $this->param['allStateProduct'] = $allStateProduct;
        $this->param['allProductType'] = $allProductType;
        $this->param['selectedBrand'] = $brandSelected;
        $this->param['selectedAttr'] = array();
        $this->param['productTypeSelected'] = $productTypeSelected;
        $this->param['query'] = $URIquery;
        $this->param['attrSelected'] = $attrSelected;
        $this->param['stateSelected'] = $stateSelected;
        $this->param['breadcrumb'] = $breadcrumb;
        view()->render('shop-search', $this->param);
    }


    /**
     * Può essere una pagina singola oppure un post
     *
     * @param $permalink
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function single($permalink)
    {
        //Prendo la pagina con permalink = al permalink passato
        $category = Category::where('permalink', $permalink)->first();
        if ($category) {
            return $this->shopList($category->permalink);
        }


        $post = \Plugins\CMS\Models\Post::where('permalink', $permalink)->where('state', 1)->first();
        // Nessun post = 404
        if (!$post) {
//            abort('404', container()->get('http404'));
            abort('404', function () {
                return view()->render('404', $this->param);
            });
        }

        $this->param['title'] = $post->meta('title') . " - Blob Video";
        $description = trim(strip_tags($post->meta('content')));
        if (strlen($description) > 300)
            $description = substr($description, 0, 300);
        $this->param['description'] = $description;


        if ($post && $post->meta('seo')) {
            $seo = unserialize($post->meta('seo'));
            if ($seo['title']) {
                $this->param['titleFacebook'] = $seo['title'] . " - Blob Video";
            }
            if ($seo['description']) {
                $description = trim(strip_tags($seo['description']));
                if (strlen($description) > 300)
                    $description = substr($description, 0, 300);
                $this->param['descriptionFacebook'] = $description;
            }
        }
        if ($post->meta("imgsocial")) {
            $this->param['imgFacebook'] = config('httpmedia') . '/' . basename($post->meta('imgsocial'));
        }

        // se post privato
        if ($post->visibility > 0) {
            if (!container()->get('authF')->isAuth(session()->get('__token'))) {
                return redirector()->route('noaccess');
            }
        }

        // code del post-type
        $post_type_code = $post->post_type->code;

        // metto nel container per utilizzare le API del tema
        container()->set('the_post', $post);


        // Se è una pagina chiamo la vista "page" altrimenti "single"
        if ($post_type_code == 'pages') {
            try {
                // se trovo una vista su l'id della pagina prendo quella
                if (view()->viewFinder->find('page_' . $post->id)) {
                    $this->param['post'] = $post;
                    return view()->render(
                        'page_' . $post->id,
                        $this->param
                    );
                }
            } catch (\InvalidArgumentException $e) {
                try {
                    // se trovo una vista sul permalink della pagina prendo quella
                    if (view()->viewFinder->find('page_' . $permalink)) {
                        $this->param['post'] = $post;
                        return view()->render(
                            'page_' . $permalink,
                            $this->param
                        );
                    }
                } catch (\InvalidArgumentException $e) {
                    // altrimenti vista generica
                    $this->param['post'] = $post;
                    return view()->render(
                        'page',
                        $this->param
                    );
                }
            }
        }
        // Post Singolo Generico
        try {
            // layout con id
            if (view()->viewFinder->find('single_' . $post->id)) {
                $this->param['post'] = $post;
                return view()->render(
                    'single_' . $post->id,
                    $this->param
                );
            }

        } catch (\InvalidArgumentException $e) {
            // layout permalink
            try {
                if (view()->viewFinder->find('single_' . $permalink)) {
                    $this->param['post'] = $post;
                    return view()->render(
                        'single_' . $permalink,
                        $this->param
                    );
                }
            } catch (\InvalidArgumentException $e) {
                // layout post-type
                try {
                    $this->param['post'] = $post;
                    if (view()->viewFinder->find('single_' . $post_type_code)) {
                        return view()->render(
                            'single_' . $post_type_code,
                            $this->param
                        );
                    }
                } catch (\InvalidArgumentException $e) {
                    $this->param['post'] = $post;
                    return view()->render(
                        'single',
                        $this->param
                    );
                }
            }
        }
    }


    /**
     * Può essere un singolo prodotto
     *
     * @param $permalink
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function product($permalink)
    {

        //Prendo la pagina con permalink = al permalink passato
        //$product = \Plugins\ECOMMERCE\Models\Product::where('permalink', $permalink)->where('status', 1)->first();
        $product = \Plugins\ECOMMERCE\Models\Product::where('permalink', $permalink)->first();
        // Nessun post = 404
        if (!$product) {
//            abort('404', container()->get('http404'));
            abort('404', function () {
                return view()->render('404', $this->param);
            });
        }


        $this->param['title'] = $product->title . " - Blob Video";
        $this->param['titleFacebook'] = $product->title;
        $this->param['url'] = "https://blobvideo.com/" . $permalink;
        $this->param['singleProduct'] = false;

        $description = trim(strip_tags($product->description));
        if (strlen($description) > 300)
            $description = substr($description, 0, 300);
        $this->param['description'] = $description . " - Blob Video";
        $this->param['descriptionFacebook'] = $description . " - Blob Video";

        if ($product->meta("imgsocial")) {
            $this->param['imgFacebook'] = config('httpmedia') . 'ecommerce/prodotti/' . basename($product->meta('imgsocial'));
        } else {
            if ($product->meta('imghighlight'))
                $this->param['imgFacebook'] = config('httpmedia') . 'ecommerce/prodotti/' . basename($product->meta('imghighlight'));
        }


        //breadcrumb
        $breadcrumb = [];
        $breadcrumbPermalink = [];
        $category = Category::find($product->id_category);
        $stepLabel = explode(' | ', $category->path_label);
        $stepId = explode(' | ', $category->path);
        for ($i = 0; $i < count($stepId); $i++) {
            $breadcrumb[$stepId[$i]] = $stepLabel[$i];
            $BreadCategory = Category::where('id', $stepId[$i])->first();
            $breadcrumbPermalink[$stepId[$i]] = $BreadCategory->permalink;
        }

        $this->formatPrice($product);

        if ($product->id_brand)
            $product->produttore = $product->brand->title;
        else
            $product->produttore = "n.d.";

        if ($product->state_product == 0) {
            $product->state_product_label = "Nuovo";
        } else if ($product->state_product == 1) {
            $product->state_product_label = "Usato";
        } else if ($product->state_product == 2) {
            $product->state_product_label = "In Arrivo ";
//            if ($product->meta('arrival_date')) {
//                $product->state_product_label = $product->meta('arrival_date');
//            }
        } else if ($product->state_product == 3) {
            $product->state_product_label = "In Rientro ";
//            if ($product->meta('arrival_date')) {
//                $product->state_product_label = $product->meta('arrival_date');
//            }
        }
        $this->param['product'] = $product;
        //prelevo la tipologia merceologica e le feature associate
        if ($product->id_category) {
            $this->param['productType'] = $product->category->productType;
        }
        $this->param['attributes'] = $product->attributes;

        $this->param['breadcrumb'] = $breadcrumb;
        $this->param['breadcrumbPermalink'] = $breadcrumbPermalink;
        return view()->render(
            'product',
            $this->param
        );


    }


    /**
     * salvo il nuovo valore nel carrello
     *
     *
     */
    public function savecarrello()
    {
        $error = '';
        $idProdotto = request()->post('id_product');
        $quantita = request()->post('quantity');

        if (!is_numeric($quantita) || $quantita < 0)
            $error .= "Quantità: valore non valido ";

        if (!is_numeric($idProdotto))
            $error .= "Prodotto: valore non valido";

        if (strlen($error) == 0) {

            $product = \Plugins\ECOMMERCE\Models\Product::where('id', $idProdotto)->where('status', 1)->first();
            $carrello = json_decode(stripcslashes($_COOKIE['checkout']));
            if ($product) {
                if ($product->quantity < $quantita && $product->type == 0)
                    $error .= "Prodotto non disponibile";
            }

        }
        if ($error != '') {
            $return = array('result' => $error);
            echo htmlspecialchars(json_encode($return), ENT_NOQUOTES);
            return;
        } else {
            if ($carrello) {
                $find = false;
                foreach ($carrello as $row) {
                    if ($row->id_product == $idProdotto) {
                        $row->quantity++;
                        $find = true;
                    }
                }
            }
            if (!$find) {
                $newProduct = array("id_product" => $idProdotto, "quantity" => $quantita);
                $carrello[] = $newProduct;
            }
            $json_carrello = json_encode($carrello);
            setcookie("checkout", $json_carrello, strtotime('+90 days'), "/");
            $return = array('result' => 1);
            echo json_encode($return);
            return;
        }


    }


    /**
     * Può essere un singolo prodotto
     *
     * @param $permalink
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public
    function checkout()
    {
        $this->param['title'] = "checkout - Blob Video";
        $allProdotti = array();
        $prezzoTotale = 0;
        if (!empty($_COOKIE['checkout'])) {
            $prodotti = json_decode(stripcslashes($_COOKIE['checkout']));
            if ($prodotti) {
                foreach ($prodotti as $prodotto) {
                    if (!$prodotto) continue;
                    $product = \Plugins\ECOMMERCE\Models\Product::where('id', $prodotto->id_product)->where('status', 1)->first();
                    $product->quantity = $prodotto->quantity;
                    if (($product->price_offer > 0)) {
                        $prezzoTotale += $product->price_offer * $product->quantity;
                        $product->price_cart = $product->price_offer;
                    } else {
                        $prezzoTotale += $product->price * $product->quantity;
                        $product->price_cart = $product->price;
                    }

                    $allProdotti[] = $product;
                }
            }
        }

        //Recupero le info della spedizione
        $subTotale = $prezzoTotale;
        $spedizione = \Plugins\ECOMMERCE\Models\Shipment::where('status', 1)->where('id', 1)->first();
        if ($prezzoTotale >= $spedizione->free) {
            $spedizione->label_cart = "Spedizione Gratis";
        } else {
            $spedizione->label_cart = "€ " . $this->format_price_front($spedizione->price);
            $spedizione->label_cart_residuo = "<i class=\"fa fa-exclamation-triangle orange\"> </i> Ti mancancano " . $this->format_price_front($spedizione->free - $subTotale) . " euro per usufruire della spedizione gratuita";
            $prezzoTotale += $spedizione->price;
        }


        $this->param['product'] = $allProdotti;
        $this->param['spedizione'] = $spedizione;
        $this->param['sub_total'] = $this->format_price_front($subTotale);
        $this->param['total'] = $this->format_price_front($prezzoTotale);

        return view()->render(
            'checkout',
            $this->param
        );
    }

    /**
     * Può essere un singolo prodotto
     *
     * @param $permalink
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public
    function proceedCheckout()
    {
        $this->param['title'] = "checkout - Blob Video";
        $id_spedizione = request()->post('spedizione');

        $pagamenti = \Plugins\ECOMMERCE\Models\Payment::where('status', 1)->get();
        $paypal = \Plugins\ECOMMERCE\Models\Payment::where('status', 1)->where('code', 'PayPal')->first();

        $user = session()->get('__user');


        $indirizzoSpedizione = null;
        $indirizzoFatturazione = null;
        if ($user) {
            $utente = unserialize($user);
            if ($utente->billing_address())
                $indirizzoFatturazione = $utente->billing_address();
            else
                $indirizzoFatturazione = new  CustomerAddress();
            if ($utente->shipping_address())
                $indirizzoSpedizione = $utente->shipping_address();
            else
                $indirizzoSpedizione = new  CustomerAddress();
        } else {
            $utente = new Customer();
            $indirizzoFatturazione = new  CustomerAddress();
            $indirizzoSpedizione = new  CustomerAddress();
        }


        if (!empty($_COOKIE['checkout'])) {
            $prodotti = json_decode(stripcslashes($_COOKIE['checkout']));
            $allProdotti = array();
            $prezzoTotale = 0;
            if ($prodotti) {
                foreach ($prodotti as $prodotto) {
                    $product = \Plugins\ECOMMERCE\Models\Product::where('id', $prodotto->id_product)->where('status', 1)->first();
                    $product->quantity = $prodotto->quantity;
                    if (($product->price_offer > 0)) {
                        $prezzoTotale += $product->price_offer * $product->quantity;
                        $product->price_cart = $product->price_offer;
                    } else {
                        $prezzoTotale += $product->price * $product->quantity;
                        $product->price_cart = $product->price;
                    }

                    $allProdotti[] = $product;
                }
            }
        }

        //Recupero le info della spedizione
        $subTotale = $prezzoTotale;

        //mi calcolo la percentuale in caso di pagamento paypal
        $prezzoPaypal = number_format($subTotale * ($paypal->cost / 100), 2);

        $spedizione = \Plugins\ECOMMERCE\Models\Shipment::where('status', 1)->where('id', $id_spedizione)->first();
        if ($id_spedizione == 1) { //Spedizione con corriere a Pagamento
            if ($prezzoTotale >= $spedizione->free) {
                $spedizione->label_cart = "Spedizione Gratis";
            } else {
                $spedizione->label_cart = $spedizione->price . " €";
                $prezzoTotale += $spedizione->price;
            }
            $spedizione->label_title = $spedizione->title;
        } else {
            $spedizione->label_cart = $spedizione->title;
            $spedizione->label_title = "Spedizione";
        }


        $this->param['product'] = $allProdotti;
        $this->param['spedizione'] = $spedizione;
        $this->param['sub_total'] = $this->format_price_front($subTotale);
        $this->param['total'] = $this->format_price_front($prezzoTotale);
        $this->param['cost_paypal'] = $this->format_price_front($prezzoPaypal);
        $this->param['total_paypal'] = $this->format_price_front($prezzoTotale + $prezzoPaypal);
        $this->param['total_paypal_form'] = $prezzoTotale + $prezzoPaypal;
        $this->param['user'] = $utente;
        $this->param['billing_address'] = $indirizzoFatturazione;
        $this->param['ship_address'] = $indirizzoSpedizione;
        $this->param['method_payment'] = $pagamenti;
        $this->param['idShip'] = $id_spedizione;


        return view()->render(
            'proceed_checkout',
            $this->param
        );


    }


    public
    function saveOrder()
    {
        $utente = $this->getUser();
        $billing_address = request()->post('billing_address');
        $order = request()->post('order');

        $error = '';
        $prodotti = json_decode(stripcslashes($_COOKIE['checkout']));
        if ($prodotti) {
            foreach ($prodotti as $prodotto) {
                $product = \Plugins\ECOMMERCE\Models\Product::where('id', $prodotto->id_product)->where('status', 1)->first();
                if ($product) {
                    if ($product->quantity < $prodotto->quantity && $product->type == 0) {
                        $error .= "Disponibilità prodotto esigua ";
                        break;
                    }

                }
            }
        } else {
            $error = "Errore di sistema, aggiorna la pagina per continuare";
        }


        if ($error != '') {
            $return = array('result' => $error);
            echo htmlspecialchars(json_encode($return), ENT_NOQUOTES);
            return;
        }


        if ($utente) {
            $order['id_customer'] = $utente->id;
        } else {
            //Creo un nuovo utente
            /**/
            $user = request()->get('customer');
            $user['email'] = $billing_address['email'];
            $user['name'] = $billing_address['name'];
            $user['surname'] = $billing_address['surname'];

            $customer = Customer::where('email', $user['email'])->first();
            if ($customer)
                $error .= 'Errore durante la fase di registrazione, email già presente ';

            //($validator->validate( $item );
            if ($user['password'] != $user['passwordRepeat'])
                $error .= 'Errore durante la creazione user, le due password immesse sono diverse';


            if ($this->isPasswordEncrypted($user)) {
                $user['password'] = md5($user['password']);
            } else {
                $newPassword = $this->generatePassword(8);
                $user['password'] = md5($newPassword);
            }

            $user['status'] = 1;
            $recordUser = Customer::saveOrUpdate($user);
            /**/
            $order['id_customer'] = $recordUser->id;
        }


        if ($error != '') {
            $return = array('result' => $error);
            echo htmlspecialchars(json_encode($return), ENT_NOQUOTES);
            return;
        } else {
            $title = '';
            //calcolo il prezzo totale


            if (!empty($_COOKIE['checkout'])) {
                $prodotti = json_decode(stripcslashes($_COOKIE['checkout']));
                $prezzoTotale = 0;
                if ($prodotti) {
                    foreach ($prodotti as $prodotto) {

                        $product = \Plugins\ECOMMERCE\Models\Product::where('id', $prodotto->id_product)->where('status', 1)->first();
                        $title .= $product->title . " ";
                        $product->quantity = $prodotto->quantity;
                        if (($product->price_offer > 0)) {
                            $prezzoTotale += $product->price_offer * $product->quantity;
                            $product->price_cart = $product->price_offer;
                        } else {
                            $prezzoTotale += $product->price * $product->quantity;
                            $product->price_cart = $product->price;
                        }
                    }
                }
            }

            $paypalPagamento = \Plugins\ECOMMERCE\Models\Payment::where('status', 1)->where('code', 'PayPal')->first();
            if ($order['payment_type'] == $paypalPagamento->id) { //Metodo Pagamento PayPal
                //mi calcolo la percentuale in caso di pagamento paypal
                $prezzoPaypal = number_format($prezzoTotale * ($paypalPagamento->cost / 100), 2);
                $prezzoTotale += $prezzoPaypal;
            }

            $speseSpedizioni = 0;

            if ($order['id_ship'] == 1) { //Spedizione con corriere a Pagamento
                $spedizione = \Plugins\ECOMMERCE\Models\Shipment::where('status', 1)->where('id', $order['id_ship'])->first();
                if ($prezzoTotale < $spedizione->free) {
                    $prezzoTotale += $spedizione->price;
                    $speseSpedizioni = $spedizione->price;
                }
            }


            //Mi creo l'oggetto di tipo ordine
            $order['date'] = date("Y-m-d");
            $order['total'] = $prezzoTotale;
            $order['title'] = $title;
            $order['cost_ship'] = $speseSpedizioni;
            $order['downloadable'] = '';
            try {

                $resultOrder = Order::saveOrUpdate($order);

                //associo i prodotti all'ordine inserito
                if ($prodotti) {
                    foreach ($prodotti as $prodotto) {
                        $product = \Plugins\ECOMMERCE\Models\Product::where('id', $prodotto->id_product)->where('status', 1)->first();
                        $product->quantity = $prodotto->quantity;
                        if (($product->price_offer > 0)) {
                            $product->price_cart = $product->price_offer;
                        } else {
                            $product->price_cart = $product->price;
                        }
                        $product_order['id_product'] = $prodotto->id_product;
                        $product_order['id_order'] = $resultOrder->id;
                        $product_order['title'] = $product->title;
                        $product_order['quantity'] = $prodotto->quantity;
                        $product_order['vat'] = 0;
                        $product_order['price'] = $product->price_cart;
                        $resultOrderProduct = OrderProduct::saveOrUpdate($product_order);
                    }
                }

                //Salvo l'indirizzo di fatturazione
                $billing_address = request()->get('billing_address');
                $billing_address['id_order'] = $resultOrder->id;
                unset($billing_address['id']);
                $result_billing_address = OrderAddress::saveOrUpdate($billing_address);

                if ((request()->post('check-address-ship')) && request()->post('check-address-ship') == 1) { // è stato inserito l'indirizzo di spedizione
                    $shipping_address = request()->get('ship_address');
                    $shipping_address['id_order'] = $resultOrder->id;
                    unset($shipping_address['id']);
                    $result_shipping_address = OrderAddress::saveOrUpdate($shipping_address);
                }

                if ($utente) {
                    $billing_address_customer = request()->get('billing_address');
                    unset($billing_address_customer['id']);
                    if ($utente->billing_address()) {
                        $billing_address_customer['id'] = $utente->billing_address()->id;
                    }
                    $billing_address_customer['id_customer'] = $utente->id;
                    unset($billing_address_customer['name']);
                    unset($billing_address_customer['surname']);
                    unset($billing_address_customer['email']);
                    CustomerAddress::saveOrUpdate($billing_address_customer);

                    if ((request()->post('check-address-ship')) && request()->post('check-address-ship') == 1) { // è stato inserito l'indirizzo di spedizione
                        $shipping_address_customer = request()->get('ship_address');
                        unset($shipping_address_customer['id']);
                        if ($utente->shipping_address()) {
                            $shipping_address_customer['id'] = $utente->shipping_address()->id;
                        }
                        $shipping_address_customer['id_customer'] = $utente->id;
                        unset($shipping_address_customer['name']);
                        unset($shipping_address_customer['surname']);
                        unset($shipping_address_customer['email']);
                        CustomerAddress::saveOrUpdate($shipping_address_customer);
                    }
                }

            } catch (\Ring\Exception\ValidationException $ex) {
                die($ex->getMessage());
            }

            //print_r($prodotti);

            if ($prodotti) {
                foreach ($prodotti as $prodotto) {
                    $product = \Plugins\ECOMMERCE\Models\Product::where('id', $prodotto->id_product)->where('status', 1)->first();
                    $product->quantity -= $prodotto->quantity;
                    $product->save();
                }
            }
            //invio la mail
            hooks()->do_action(CRM_BUY_SEND_MAIL, $resultOrder);
            //hooks()->do_action( CRM_BUY_SEND_MAIL_ADMIN, $resultOrder );
            setcookie("checkout", '', strtotime('+90 days'), "/");
            $param = array('result' => $resultOrder->id, 'order' => $resultOrder);
            return $param;
        }

        $return = array('result' => "Errore durante l'operazione ");
        return $param;


    }

    public function pagamentoPaypal()
    {
        // Read POST data
        // reading posted data directly from $_POST causes serialization
        // issues with array data in POST. Reading raw POST data from input stream instead.
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);
        }
// read the post from PayPal system and add 'cmd'
        $req = 'cmd=_notify-validate';
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }
// Post IPN data back to PayPal to validate the IPN data is genuine
// Without this step anyone can fake IPN data
        if (USE_SANDBOX == true) {
            $paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        } else {
            $paypal_url = "https://www.paypal.com/cgi-bin/webscr";
        }
        $ch = curl_init($paypal_url);
        if ($ch == FALSE) {
            return FALSE;
        }
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        if (DEBUG == true) {
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
        }
// CONFIG: Optional proxy configuration
//curl_setopt($ch, CURLOPT_PROXY, $proxy);
//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
// Set TCP timeout to 30 seconds
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
// CONFIG: Please download 'cacert.pem' from "https://curl.haxx.se/docs/caextract.html" and set the directory path
// of the certificate as shown below. Ensure the file is readable by the webserver.
// This is mandatory for some environments.
//$cert = __DIR__ . "./cacert.pem";
//curl_setopt($ch, CURLOPT_CAINFO, $cert);
        $res = curl_exec($ch);
        if (curl_errno($ch) != 0) { // cURL error
            if (DEBUG == true) {
                error_log(date('[Y-m-d H:i e] ') . "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);
            }
            curl_close($ch);
            exit;
        } else {
            // Log the entire HTTP response if debug is switched on.
            if (DEBUG == true) {
                error_log(date('[Y-m-d H:i e] ') . "HTTP request of validation request:" . curl_getinfo($ch, CURLINFO_HEADER_OUT) . " for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);
                error_log(date('[Y-m-d H:i e] ') . "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);
            }
            curl_close($ch);
        }

// Inspect IPN validation result and act accordingly
// Split response headers and payload, a better way for strcmp
        $tokens = explode("\r\n\r\n", trim($res));
        $res = trim(end($tokens));
        if (strcmp($res, "VERIFIED") == 0) {
            $idOrdine = $_POST['custom'];
            //if(isset($idOrdine) && is_numeric($idOrdine)) {
            // check whether the payment_status is Completed
            // check that txn_id has not been previously processed
            // check that receiver_email is your PayPal email
            // check that payment_amount/payment_currency are correct
            // process payment and mark item as paid.
            // assign posted variables to local variables
            $item_name = $_POST['item_name'];
            //$item_number = $_POST['item_number'];
            //$payment_status = $_POST['payment_status'];
            //$payment_amount = $_POST['mc_gross'];
            //$payment_currency = $_POST['mc_currency'];
            $txn_id = $_POST['txn_id'];
            //$receiver_email = $_POST['receiver_email'];
            $payer_email = $_POST['payer_email'];
            $userPaypal = $_POST['first_name'] . ' ' . $_POST['last_name'];


            $order = Order::find($idOrdine);
            $order->email_paypal = $payer_email;
            $order->code_payment = $txn_id;
            $order->user_paypal = $userPaypal;
            $datePay = date_create($_POST['payment_date']);
            $dataPagamento = date_format($datePay, 'Y-m-d H:i:s');
            $order->payment_date = $dataPagamento;
            $order->status = 1;
            $order->payment_status = 1;
            $order->save();

            //}
            /*
              $ordine->StatoPagamento = 1;
              $ordine->IdTransazione = $txn_id;
              $ordine->DataPagamento = date('Y-m-d H:i:s');
              //calcolo il nuovo numero della fattura
              $ordine->CodiceFattura = $this->calcolaNumeroFattura();
              $ordine->update(); */
            //hooks()->do_action(CRM_BUY_SEND_MAIL, $order);

        } else if (strcmp($res, "INVALID") == 0) {
            // log for manual investigation
            // Add business logic here which deals with invalid IPN messages
            if (DEBUG == true) {
                error_log(date('[Y-m-d H:i e] ') . "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE);
            }
        }

        $this->param['permalink'] = 'success-checkout';

        return view()->render('single-page', $this->param);


    }

    public
    function userOrderInfo()
    {
        $this->param['title'] = "ordini - Blob Video";
        $id = request()->get('id_order');
        if (isset($id) && $id) {
            $order = Order::find($id);
            $this->formatPriceOrder($order);
            $this->param['record'] = $order;
        } else {
            $this->param['record'] = new Order();
        }

        return view()->render('userorderinfo', $this->param);
    }


    /**
     * Mostra una lista archivio di una categoria di un post
     *
     * @param $code_post_type
     * @param $code_cat
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public
    function archive($code_post_type, $code_cat = null)
    {

        //Prendo tutti i post del post type e categoria data
        $postType = \Plugins\CMS\Models\PostType::where('code', $code_post_type)->first();

        // Nessun post_type = 404
        if (!$postType) {
//            abort('404', container()->get('http404'));
            abort('404', function () {
                return view()->render('404', $this->param);
            });
        }

        if ($code_cat) {
            $cat = \Plugins\CMS\Models\CatPostType::where('id_post_type', $postType->id)->where('code', $code_cat)->first();
            // Nessuna cat = 404
            if (!$cat) {
//                abort('404', container()->get('http404'));
                abort('404', function () {
                    return view()->render('404', $this->param);
                });
            }
            $posts = $cat->posts()->where('state', 1)->orderBy('publish_date', 'DESC')->get();
        } else {
            // SEO e posts del post type
            $cat = array(
                'seo' => array(
                    'title' => $postType->name . ' - ' . config('appname'),
                    'description' => 'Lista ' . $postType->name
                )
            );
            $posts = $postType->posts()->where('state', 1)->orderBy('publish_date', 'DESC')->get();
        }

        if ($code_cat) {
            $code_cat = '_' . $code_cat;
        } else {
            $code_cat = '';
        }

        try {
            // se trovo il layout della categoria
            if (view()->viewFinder->find('archive_' . $code_post_type . $code_cat)) {
                return view()->render(
                    'archive_' . $code_post_type . $code_cat,
                    array(
                        'post' => $cat,
                        'posts' => $posts
                    )
                );
            }
        } catch (\InvalidArgumentException $e) {
            try {
                // se trovo il layout del post type
                if (view()->viewFinder->find('archive_' . $code_post_type)) {
                    return view()->render(
                        'archive_' . $code_post_type,
                        array(
                            'post' => $cat,
                            'posts' => $posts
                        )
                    );
                }
            } catch (\InvalidArgumentException $e) {
                // layout archivio generico
                return view()->render(
                    'archive',
                    array(
                        'post' => $cat,
                        'posts' => $posts
                    )
                );
            }
        }
    }

    /**
     * Ricerca nei post
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public
    function search()
    {
        $q = request()->query('q');

        //Prendo tutti i post cercando nel titolo o nel contenuto
        $searchResults = Post::search($q)->get();

        // Carico la view
        return view()->render(
            'search',
            array(
                'post' => array(
                    'seo' => array(
                        'title' => 'Risultati di ricerca - ' . config('appname'),
                        'description' => config('appdescription')
                    )
                ),
                'posts' => $searchResults
            )
        );
    }

    /**
     * Invio mail contatti
     * @return array
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public
    function mailc()
    {
        $form = request()->all();
        $nome_mittente = $form['cf-email'];
        $mail_mittente = $form['cf-email'];
        $mail_destinatario = config('mailcontact');

        // definisco il subject ed il body della mail
        $mail_oggetto = "Mail Contatto";
        $mail_corpo = "Nome: {$form['cf-name']}\nMail: {$form['cf-email']}\nNumero di tel: {$form['cf-tel']}\n\nMessaggio:\n\n" . $form['cf-mex'];

// aggiusto un po' le intestazioni della mail
// E' in questa sezione che deve essere definito il mittente (From)
// ed altri eventuali valori come Cc, Bcc, ReplyTo e X-Mailer
        $mail_headers = "From: " . $nome_mittente . " <" . $mail_mittente . ">\r\n";
        $mail_headers .= "Reply-To: " . $mail_mittente . "\r\n";
        $mail_headers .= "X-Mailer: PHP/" . phpversion();

        if (mail($mail_destinatario, $mail_oggetto, $mail_corpo, $mail_headers)) {
            return ["result" => true];
        } else {
            return ["result" => false];
        }
    }

    /**
     * Login View and action
     *
     * @return mixed
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public
    function login()
    {
        $this->param['title'] = "login - Blob Video";
        if (request()->isMethod("post")) {
            $post = request()->all();
            if (container()->get('authF')->login($post)) {
                return redirector()->route('frontpage');
            } else {
                return redirector()->route('loginp', array('error' => 'error'));
            }
        } else {
            return redirector()->route('loginp');
        }
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public
    function registrationPage($error = null)
    {
        $this->param['title'] = "registrazione - Blob Video";
        $this->param['error'] = $error;
        view()->render(
            'registerpage',
            $this->param
        );
    }

    public
    function userArea()
    {
        $this->param['title'] = "area Utente - Blob Video";
        if (!$this->getUser()) {
            return redirector()->route('loginp');
        }

        view()->render(
            'user',
            $this->param
        );
    }

    public
    function userData()
    {
        $this->param['title'] = "area Utente - Blob Video";
        if (!$this->getUser()) {
            return redirector()->route('loginp');
        }
        $utente = $this->getUser();
        if ($utente) {
            if (isset($utente->id) && $utente->id) {
                $this->param['record'] = Customer::find($utente->id);
            }
        }

        view()->render(
            'userdata',
            $this->param
        );
    }

    public
    function userDataSave()
    {
        if (!$this->getUser()) {
            return redirector()->route('loginp');
        }
        $param = array('result' => "Errore");
        $item = request()->get('item');
        //$validator = new \Modules\Backend\Classes\ValidationUsers();

        try {

            $record = Customer::saveOrUpdate($item);
            //save meta
            $this->saveCustomerMeta($record);
            //save address
            $this->saveAddress($record);
            $param = array('result' => $record->id);
        } catch (\Ring\Exception\ValidationException $ex) {
            die($ex->getMessage());
        }

        return $param;
    }

    public function userOrder()
    {
        $this->param['title'] = "area Utente - Blob Video";
        if (!$this->getUser()) {
            return redirector()->route('loginp');
        }
        $utente = $this->getUser();
        if ($utente) {
            if (request()->get('type') && request()->get('type') == 1) {
                $this->param['table'] = Order::where('id_customer', $utente->id)->where('downloadable', '=', '')->orderBy('id', 'DESC')->get();
            } else {
                $this->param['table'] = Order::where('id_customer', $utente->id)->where('downloadable', '!=', '')->orderBy('id', 'DESC')->get();
            }
        }

        view()->render(
            'userlistorder',
            $this->param
        );
    }


    public function saveAddress(\Plugins\CRM\Customer\Models\Customer $record)
    {
        $shipping_address = request()->get('shipping_address');
        $shipping_address['id_customer'] = $record->id;

        $billing_address = request()->get('billing_address');
        $billing_address['id_customer'] = $record->id;

        $shipping_obj = CustomerAddress::saveOrUpdate($shipping_address);
        $billing_address = CustomerAddress::saveOrUpdate($billing_address);
    }

    /**
     * Salva i meta
     *
     * @param Customer $record
     */
    public
    function saveCustomerMeta(\Plugins\CRM\Customer\Models\Customer $record)
    {
        $items = request()->get('meta');

        if (request()->get('language') && request()->get('language') !== '') {
            $iso = request()->get('language');
        } else {
            $iso = config('locale');
        }

        // Salvo il meta
        foreach ($items as $key => $value) {
            // Cancello le tassonomie associate
            CustomerMeta::where('id_customer', '=', $record->id)->where('iso', $iso)->where('meta_key', $key)->delete();

            $metaArray['meta_key'] = $key;
            if (is_array($items[$key])) {
                $metaArray['value'] = serialize($value);
            } else {
                $metaArray['value'] = $value;
            }
            $metaArray['id_customer'] = $record->id;
            $metaArray['iso'] = $iso;
            CustomerMeta::create($metaArray);
        }
    }


    public
    function getUser()
    {
        if (session()->get('__user')) {
            $user = session()->get('__user');
            return unserialize($user);
        }
        return false;
    }

    public function wishlist()
    {
        $breadcrumb = array("wishlist");
        $product = array();
        $wishList = ($_COOKIE['wishlist']);
        $arrayWishList = json_decode($wishList);
        if ($arrayWishList && is_array($arrayWishList) && count($arrayWishList) > 0) {
            foreach ($arrayWishList as $singleWish) {
                if ($singleWish > 0) {
                    $product[] = Product::where('status', 1)->where('quantity', '>', 0)->where('id', $singleWish)->first();
                }
            }

            //$product = Product::where('status', 1)->where('quantity', '>', 0)->whereIn('id', $wishList)->get();
        }

        $this->param['products'] = $product;
        $this->param['breadcrumb'] = $breadcrumb;
        view()->render(
            'wishlist',
            $this->param
        );
    }


    public
    function register()
    {

        $item = request()->get('customer');
        $metaUser = request()->get('meta');

        $gResp = request()->get('g-recaptcha-response');
        if (!$gResp) {
            return redirector()->route('registrationp', array('error_register' => 'Errore durante la fase di registrazione '));
        }
        if (!isset($metaUser['phone']) || !$metaUser['phone'] || (isset($metaUser['phone']) && strlen(trim($metaUser['phone'])) == 0)) {
            abort(404, function () {
                die();
            });
        }

        if (strpos($item['email'], '@burjanet.') !== false || strpos($item['email'], '@yandex.') !== false) {
            abort(404, function () {
                die();
            });
        }

        $customer = Customer::where('email', trim(strtolower($item['email'])))->first();
        if ($customer)
            return redirector()->route('registrationp', array('error_register' => 'Errore durante la fase di registrazione, email già presente '));

        //($validator->validate( $item );
        if ($item['password'] != $item['passwordRepeat'])
            return redirector()->route('registrationp', array('error_register' => 'Errore durante la fase di registrazione, le due password immesse sono diverse'));


        if ($this->isPasswordEncrypted($item)) {
            $item['password'] = md5($item['password']);
        } else {
            $newPassword = $this->generatePassword(8);
            $item['password'] = md5($newPassword);
        }

        $item['status'] = 1;
        $record = Customer::saveOrUpdate($item);
        $this->saveCustomerMeta($record);
        //save address
        $billing_address = array();

        $billing_address['phone'] = $metaUser['phone'];
        $billing_address['id_customer'] = $record->id;
        $billing_address['type'] = 2;
        $billing_address['selected'] = 1;
        CustomerAddress::saveOrUpdate($billing_address);
        if ($record) {
            hooks()->do_action(CRM_CUSTOMER_SEND_MAIL, $item);
            return redirector()->route('registrationp', array('error_register' => 'ok'));
        } else
            return redirector()->route('registrationp', array('error_register' => 'Errore durante la fase di registrazione '));

    }

    public
    function savePassword()
    {

        $item = request()->get('customer');

        $error = '';
        $customer = Customer::where('id', $item['id'])->first();
        if (!$customer)
            $error .= 'Email non presente in archivio';

        //($validator->validate( $item );
        if ($item['password'] != $item['passwordRepeat'])
            $error .= 'Errore durante la fase di registrazione, le due password immesse sono diverse';


        if ($this->isPasswordEncrypted($item)) {
            $item['password'] = md5($item['password']);
        } else {
            $newPassword = $this->generatePassword(8);
            $item['password'] = md5($newPassword);
        }

        if ($error != '') {
            $return = array('result' => $error);
        } else {
            unset($item['passwordRepeat']);
            $record = Customer::saveOrUpdate($item);
            if ($record)
                $return = array('result' => 1);
            else
                $return = array('result' => -1);
        }


        return json_encode($return);


    }


    public function sendContactForm()
    {
        $item['name'] = request()->get('name_contact');
        $item['email'] = request()->get('email_contact');
        $item['msg'] = request()->get('message_contact');

        $gResp = request()->get('g-recaptcha-response');
        if ($gResp) {
            hooks()->do_action(CRM_CONTACT_SEND_MAIL, $item);
        } else {
            $return = array('result' => 0);
            return json_encode($return);
        }
        /*
             if($ret['result'])
                 $return = array('result' => 1);
             else
                 $return = array('result' => -1);*/
        $return = array('result' => 1);
        return json_encode($return);
    }


    public function recuperaPassword()
    {
        $item['email'] = request()->get('emailRecupera');
        $error = '';
        if ($item['email'] == '') {
            $error .= " Email: campo obbligatorio";
        }
        if (strlen($item['email']) > 250) {
            $error .= " Email: lunghezza massima 250 caratteri";
        }
        if (!filter_var($item['email'], FILTER_VALIDATE_EMAIL)) {
            $error .= "Email: campo non valido";
        }


        $customer = Customer::where('email', $item['email'])->first();

        if (!$customer)
            $error .= " Email: indirizzo email non presente in archivio";

        if ($error != '') {
            $return = array('result' => $error);
            echo htmlspecialchars(json_encode($return), ENT_NOQUOTES);
            return;
        } else {
            hooks()->do_action(CRM_RECUPERA_PASSWORD_SEND_MAIL, $customer);
            $return = array('result' => 1);
            return json_encode($return);
        }
        $return = array('result' => "Errore generico");
        return json_encode($return);
    }

    public
    function recoveryPasswordPage($id_customer)
    {
        $this->param['id'] = $id_customer;
        view()->render(
            'recoverypasswordpage',
            $this->param
        );
    }


    public
    function isPasswordEncrypted($item)
    {
        if (isset ($item['password']) && $item['password']) {
            return true;
        }

        return false;
    }

    /**
     * Generate password
     *
     *
     *
     * @return String value generate password
     */
    function generatePassword($length = 8, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
    {
        return substr(str_shuffle($chars), 0, $length);
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public
    function loginPage($error = null)
    {
        $fb = new \Facebook\Facebook([
            'app_id' => '2198511690420173',
            'app_secret' => '6ea6cc4fc2157491901cfe2eec256ec0',
            'default_graph_version' => 'v2.2',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions
        $this->param['urlfb'] = $helper->getLoginUrl('https://' . $_SERVER['SERVER_NAME'] . '/auth/facebook', $permissions);
        $this->param['error'] = $error;
        view()->render(
            'loginpage',
            $this->param
        );
    }

    /**
     * Logout
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public
    function logout()
    {
        container()->get('auth')->logout();
        /*$path = request()->get('ref');

        return redirector()->to($path);*/
        return redirector()->route('frontpage');
    }

    /**
     * Risorva privata
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public
    function noaccess()
    {
        view()->render(
            'noaccess',
            array()
        );
    }

    public function registerMailChimp()
    {


        if (request()->get('subscribe_email') && request()->get('subscribe_email') !== '') {
            $mail = request()->get('subscribe_email');
        } else {
            $return = array('result' => -1);
            return json_encode($return);
        }


        $list_id = '1f99424714';
        $result = container()->get('mailChimp')->post("lists/$list_id/members", [
            'email_address' => $mail,
            'status' => 'subscribed',
        ]);

        if ($result['status'] = '400' || $result['status'] = 'subscribed')
            $return = array('result' => 1);
        else
            $return = array('result' => -1);

        return json_encode($return);
    }

    public
    function formatPrice($record)
    {
        if (isset($record->price) && is_numeric($record->price))
            $record->price_label = number_format($record->price, 2, ',', ' ');
        if (isset($record->price_offer) && is_numeric($record->price_offer))
            $record->price_offer_label = number_format($record->price_offer, 2, ',', ' ');
    }

    public
    function formatPriceOrder($record)
    {
        if (isset($record->total) && is_numeric($record->total))
            $record->total_label = number_format($record->total, 2, ',', ' ');
    }

    public function authFacebook()
    {
        $fb = new \Facebook\Facebook([
            'app_id' => '2198511690420173',
            'app_secret' => '6ea6cc4fc2157491901cfe2eec256ec0',
            'default_graph_version' => 'v2.10',
            //'default_access_token' => '2198511690420173|DajgH_u4ke24E_cRO80pt0g1SLs', // optional
        ]);

        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (!isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }

// The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();

// Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);

// Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId('2198511690420173'); // Replace {app-id} with your app id
// If you know the user ID this access token belongs to, you can validate it here
//$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();

        if (!$accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
                exit;
            }
        }

        $_SESSION['fb_access_token'] = (string)$accessToken;

        $response = $fb->get('/me?fields=id,name,email', (string)$accessToken);
        $user = json_decode($response->getBody());

        $userRegistred = Customer::where('email', $user->email)->first();
        if ($userRegistred) {
            // login
            if (container()->get('authF')->loginFromFacebook($user->email)) {
                return redirector()->route('frontpage');
            } else {
                return redirector()->route('loginp', array('error' => 'error'));
            }
        } else {
            // register
            $cre = explode(' ', $user->name);
            $item['name'] = $cre[0];
            $item['surname'] = $cre[1];
            $item['email'] = $user->email;
            $item['password'] = md5($user->email);
            $item['status'] = 1;
            $record = Customer::saveOrUpdate($item);

            if ($record) {
                if (container()->get('authF')->loginFromFacebook($user->email)) {
                    return redirector()->route('frontpage');
                } else {
                    return redirector()->route('loginp', array('error' => 'error'));
                }
            } else {
                return redirector()->route('loginp', array('error' => 'error'));
            }
        }
// User is logged in with a long-lived access token.
// You can redirect them to a members-only page.
//header('Location: https://example.com/members.php');
    }

    public function facebookFeed()
    {
        $products = Product::where("facebook", 1)->get();
        $xml = <<<DOT
<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel> 
    <title>BlobVideo Catalogo</title>
    <description>Feed per Facebook</description> 
    <link>https://blobvideo.com</link>
    <atom:link href="https://blobvideo.com/feed-facebook" rel="self" type="application/rss+xml" />
DOT;

        foreach ($products as $product) {
            $category = $product->category;
            $typology = $product->category->productType->title;
            $image = config('httpmedia'). 'ecommerce/prodotti/' . basename($product->meta('imghighlight'));
            $color = '';
            $title =  html_entity_decode(strip_tags(ucfirst(strtolower($product->title))));
            if($product->state_product == 1){
                if($product->grade == 'A'){
                    $condition = 'used_like_new';
                } elseif($product->grade == 'AB'){
                    $condition = 'used_good';
                } else {
                    $condition = 'used';
                }
            } else {
                $condition = 'new';
            }
            $avaiability = $product->quantity > 0 ? "in stock" : "out of stock";
            $price = number_format($product->price,'2','.', '');
            $description = html_entity_decode(strip_tags($product->description));
            $priceOffer = $product->offer ? number_format($product->price_offer,'2','.', '') . " EUR" : '';
            $priceSales = $product->offer ? $priceOffer : $price;
            $brand = ucfirst(strtolower($product->brand->title));
            $attributesAdd = '';
            //$item_group_id = $category->id.'_'.str_replace(' ','-',$category->title);
            $item_group_id = explode('-',$product->ean)[0];
            foreach ($product->attributes as $attr){
                $title_feat = $attr->feature->title;
                if(strtolower($title_feat) == 'colore'){ $color =  $attr->title; continue; }
                if(strpos(strtolower($title_feat), 'compatibilità') !== false){ continue; }
                    $attr->title = str_replace(',','.',$attr->title);
                    $attributesAdd .= <<<DOT
<additional_variant_attribute>
                <label>$title_feat</label>
                <value>$attr->title</value>
            </additional_variant_attribute>
DOT;
            };
            $xml .= <<<DOT
<item>
    <g:item_group_id>$item_group_id</g:item_group_id>
    <g:google_product_category>Electronics</g:google_product_category>            
    <g:id>$product->id</g:id>
    <g:title>$title</g:title>            
    <g:description>$description</g:description>
    <g:link>https://blobvideo.com/$category->permalink/$product->permalink</g:link>
    <g:image_link>https:$image</g:image_link>  
    
    <color>$color</color>           
            
    $attributesAdd
            
    <g:brand>$brand</g:brand>
    <g:condition>$condition</g:condition>  
    
    <g:availability>$avaiability</g:availability>
    
    <g:product_type>$typology</g:product_type>
    
    <g:price>$price</g:price>
    <g:sale_price>$priceSales</g:sale_price>
    <g:offer_price>$priceOffer</g:offer_price>
</item>
DOT;

        }

        $xml .= <<<DOT
</channel>
</rss>
DOT;
        header("Content-type: text/xml; charset=utf-8");
        echo $xml;
    }


    function format_price_front($val)
    {
        return number_format($val, 2, ',', '.');
    }


}