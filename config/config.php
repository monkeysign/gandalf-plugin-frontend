<?php
return [
	"name"   => 'Frontend',
	"class"  => \Plugins\Frontend\Plugin::class,
	"routes" => __DIR__ . '/../routes/routes.php',
	"theme"  => [
		"name"  => config( 'frontend_theme' ),
		"folder" => __DIR__ . '/../../../themes/' . config( 'frontend_theme' ) . '/',
		"url"    => '//' . $_SERVER['SERVER_NAME'] . '/' . config( 'folder' ) . '/themes/' . config( 'frontend_theme' ) . '/'
	]
];