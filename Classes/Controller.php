<?php

namespace Plugins\Frontend\Classes;

use Plugins\ECOMMERCE\Models\Category;
use Plugins\ECOMMERCE\Models\ProductType;

class Controller
{

    public $param;

    public function __construct(\Illuminate\Http\Request $request)
    {
        // Carico le impostazione del tema
        $conf = require __DIR__ . '/../config/config.php';
        require $conf['theme']['folder'] . 'config/load.php';

        /**
         * Aggiungo il path delle views di questo plugin grazie al filtro "views_path" che si trova nel controller generale
         */
        hooks()->add_filter(APP_VIEWS_PATH, function ($views) use ($conf) {
            return array_merge([
                __DIR__ . '/../views/',
                $conf['theme']['folder'] . 'views/'
            ], $views);
        });
    }

    public function initTemplate()
    {
        //prelevo l'ultima news da inserire in footer
        $postType = \Plugins\CMS\Models\PostType::where('code', 'articoli')->first();
        $cat = array(
            'seo' => array(
                'title' => $postType->name . ' - ' . config('appname'),
                'description' => 'Lista ' . $postType->name
            )
        );
        $posts = $postType->posts()->where('state', 1)->orderBy('publish_date', 'DESC')->first();

        //crea un nuovo oggetto XML DOM
//        $xmldom = new \DOMDocument();
//
////carica il contenuto del feed presente al link indicato
//        $xmldom->load("https://blobvideo.com/blog/feed");
//
////recupera il nodo rappresentato da <item>
//        $nodo = $xmldom->getElementsByTagName("item");
//
//        $i = 0;
//        // Estraggo il contenuto dei singoli tag del nodo <item>
//        $titolo = $nodo->item($i)->getElementsByTagName("title")->item(0)->childNodes->item(0)->nodeValue;
//        $collegamento = $nodo->item($i)->getElementsByTagName("link")->item(0)->childNodes->item(0)->nodeValue;
//        $descrizione = $nodo->item($i)->getElementsByTagName("description")->item(0)->childNodes->item(0)->nodeValue;

    $titolo = 'Le differenze tra iPhone X ed iPhone XS: Ecco cosa cambia';
    $collegamento = 'https://www.blobvideo.com/blog/apple/le-differenze-tra-iphone-x-ed-iphone-xs-ecco-cosa-cambia/';
    $descrizione = 'Quali sono le differenze tra iPhone X ed iPhone XS / iPhone XS Max? Chi già possiede un iPhone X probabilmente si chiederà se valga la pena acquistare uno dei nuovi smartphone Apple.';

        //parametri facebook

        $this->param['titleFacebook'] = "Blob Video";
        $this->param['descriptionFacebook'] = "Vendiamo prodotti Nuovi ed Usati con Garanzia.Ci occupiamo di Videogiochi, Console, Dispositivi Apple, Accessori di Alta Qualità, Gadget e Contenuti Digitali.";
        $this->param['imgFacebook'] = 'https:' . asset('assets/images/') . "icons/blobvideo-share.jpg";

        $this->param['title'] = "Blob Video";
        $this->param['description'] = "Vendiamo prodotti Nuovi ed Usati con Garanzia.Ci occupiamo di Videogiochi, Console, Dispositivi Apple, Accessori di Alta Qualità, Gadget e Contenuti Digitali.";

        $arrayCatPrimoLiv = array();
        $arrayCatSecLiv = array();
        $this->param['post_footer'] = $posts;
        $this->param['categorieRoot'] = Category::where('status', 1)->where('level', 0)->orderBy('orders', 'asc')->get();
        $this->param['tipologie'] = ProductType::where('status', 1)->orderBy('orders', 'asc')->get();

        $this->param['blog_title'] = $titolo;
        $this->param['blog_link'] = $collegamento;
        $this->param['blog_description'] = substr(strip_tags($descrizione), 0, 68);


    }

}