<?php

/**
 * Class for the Auth to the App
 *
 * @author   Tartaglia Riccardo <tartaglia.riccardo@gmail.com>
 */

namespace Plugins\Frontend\Classes;

use Lcobucci\JWT\Builder as JWTBuilder,
	Lcobucci\JWT\Parser as JWTParser,
	Lcobucci\JWT\ValidationData,
	\Illuminate\Database\Eloquent\ModelNotFoundException as UserNotFoundException;

class AuthManager {

	/**
	 * User Session
	 * @var \Symfony\Component\HttpFoundation\Session\Session
	 */
	private $session;

	/**
	 * User Model
	 * @var \Illuminate\Database\Eloquent\Model
	 */
	private $model;

	/**
	 * Builder Token
	 * @var \Lcobucci\JWT\Configuration
	 */
	private $config;

	/**
	 * Private string for token generate
	 * @var \Lcobucci\JWT\Parser
	 */
	private $privateID = 'bLbVd87as';

	/**
	 * Costruttore
	 */
	public function __construct($Session, $Model) {
		$this->session = $Session;
		$this->model = $Model;
		$this->builder = new JWTBuilder();
		$this->parser = new JWTParser();
	}

	/**
	 * Build JWS Token
	 * @param type $user
	 * @return type
	 */
	private function buildToken($user) {
		$token = $this->builder
			/* ->setIssuer('http://example.com') // Configures the issuer (iss claim)
			  ->setAudience('http://example.org') // Configures the audience (aud claim) */
			->setId($this->privateID, true) // Configures the id (jti claim), replicating as a header item
			->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
			->setNotBefore(time()) // Configures the time that the token can be used (nbf claim)
			->setExpiration(time() + 36000) // Configures the expiration time of the token (exp claim)
			->set('uid', $user->id) // Configures a new claim, called "uid"
			->set('role', $user->role) // Configures a new claim, called "ruolo"
            ->set('frontend', 'utente') // Configures a new claim, called "ruolo"
			->getToken(); // Retrieves the generated token
		return $token;
	}

	/**
	 * Check se il permesso è valido
	 * @return boolean
	 */
	public function isAuth($tokensend) {
		if(!$tokensend) return false;
		$token = $this->parser->parse((string) $tokensend);

		$data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
		$data->setId($this->privateID);
		if ($token->validate($data)){
			return true;
		} else {
			$this->session->clear();
			return false;
		}
	}

	/**
	 * Login
	 */
	public function login($credential) {
		try {
			$user = \Plugins\CRM\Customer\Models\Customer::where('email', '=', $credential['email'])->where('status','=', 1)->firstOrFail();
		} catch (UserNotFoundException $exc) {
			return false;
		}

		if (md5($credential['password']) == $user->password) {
			$this->session->set('__user', serialize($user));
			$token = $this->buildToken($user);
			$this->session->set('__token', $token);
			return true;
		}
		return false;
	}

    /**
     * Login
     */
    public function loginFromFacebook($email) {
        try {
            $user = \Plugins\CRM\Customer\Models\Customer::where('email', '=', $email)->where('status','=', 1)->firstOrFail();
        } catch (UserNotFoundException $exc) {
            return false;
        }
            $this->session->set('__user', serialize($user));
            $token = $this->buildToken($user);
            $this->session->set('__token', $token);
            return true;
    }

	/**
	 * Logout
	 */
	public function logout() {
		$this->session->clear();
		return true;
	}

}