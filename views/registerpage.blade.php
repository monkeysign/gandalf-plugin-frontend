@extends('inc.layout')

@section('content')

    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <li class="trail-item">
                            <a href="#" title="">Home</a>
                            <span><img src="{{asset('assets/images/')}}icons/arrow-right.png" alt=""></span>
                        </li>
                        <li class="trail-end">
                            <a href="#" title="">Registrazione</a>
                        </li>
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->


    <section class="flat-account background">
        <div class="container">
            <div class="row">
                <div class="col-md-7">

                    <div class="form-login" style="height: auto">
                        <div class="title text-left">
                            <h3>Registrazione Gratuita</h3>
                            <p>Se sei un Utente Registrato <a href="{{path_for('loginp',array())}}" title="login">Accedi al tuo account</a>, altrimenti Compila i campi sottostanti per registrarti.</p>
                            <hr>
                        </div>

                        @if(isset($error) && $error && $error!='ok')
                            <div>
                                <h4 class="text-danger"><strong>Errore nella registrazione: </strong>{{$error}}.</h4>
                                <br>
                            </div>
                        @elseif(isset($error) && $error && $error=='ok')
                            <div>
                                <h4 class="text-success"><strong>Registrazione eseguita correttamente</strong></h4>
                                <br>
                            </div>
                        @endif

                        <form method="POST" name="registrazione" action="{{path_for('register')}}">
                            <div class="row">
                                <div class="col-md-6 mb-3 mb-md-0">
                                    <h3>Nome*</h3>
                                    <input type="text" name="customer[name]" required placeholder="Nome" class="form-control mt-1">
                                </div>
                                <div class="col-md-6 mb-3 mb-md-0">
                                    <h3>Cognome*</h3>
                                    <input type="text" name="customer[surname]" required placeholder="Cognome" class="form-control mt-1">
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-6 mb-3 mb-md-0">
                                    <h3>Email*</h3>
                                    <input type="email" name="customer[email]" required placeholder="Email" class="form-control mt-1">
                                </div>
                                <div class="col-md-6 mb-3 mb-md-0">
                                    <h3>Telefono*</h3>
                                    <input type="text" name="meta[phone]" required placeholder="Telefono" class="form-control mt-1">
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-6 mb-3 mb-md-0">
                                    <h3>Password*</h3>
                                    <input type="password" name="customer[password]" required placeholder="Password" class="form-control mt-1">
                                </div>
                                <div class="col-md-6 mb-3 mb-md-0">
                                    <h3>Conferma Password*</h3>
                                    <input type="password" name="customer[passwordRepeat]" required placeholder="Ripeti Password" class="form-control mt-1">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-left mb-3 mb-md-0">
                                    <input type="checkbox"  required name="informativa" style="opacity: 1"/>Ho letto ed accetto <a class="active" target="u_blank" href="{{ path_for('single-page', ['permalink' => 'privacy']) }}">Privacy e Cookie Policy*</a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-center mb-3 mb-md-0">
                                    <div class="g-recaptcha" data-sitekey="6LenNKQUAAAAAEA9ddKOEQ8P6DGDUHBUPTPMdM6i"></div>
                                    <button type="submit" class="btn mr-3 mt-3 default-color"> Registrati </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <p class="small">I campi contrassegnati con * sono da considerarsi obbligatori</p>
                                </div>
                            </div>
                        </form>
                    </div><!-- /.form-login -->
                </div><!-- /.col-md-8 -->
                <div class="col-md-5 right-form-login ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Vantaggi riservati agli utenti registrati</h3>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-checkbox style2">
                                <div class="checkbox">
                                    <input type="checkbox" name="category" checked>
                                    <label for="gionee">Crea e gestisci la tua lista dei desideri</label>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox"  name="category" checked>
                                    <label for="gionee">Ricevi avvisi quando un prodotto torna disponibile</label>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox"  name="category" checked>
                                    <label for="gionee">Velocizza gli acquisti con le tue preferenze di spedizione</label>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox" name="category" checked>
                                    <label for="gionee">Tieni traccia dei tuoi ordini</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col-md-4 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-account -->
    <section class="flat-row flat-iconbox style3">
        <div class="container">
            <div class="row">
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/banconota.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Spedizioni Gratis</h4>
                                <p>per ordini superiori a 79&euro;</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/consegna.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Consegne 24/48 H</h4>
                                <p>con corriere espresso</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img style="width:45px;" src="{{asset('assets/images/')}}icons/pagamentisicuri.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Pagamenti Sicuri</h4>
                                <p>con PayPal e Bonifico</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/utente.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Registrati al Sito</h4>
                                <p>per Offerte e Promozioni</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/fumetto.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Contattaci</h4>
                                <p>per maggiori informazioni</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-iconbox -->
@endsection