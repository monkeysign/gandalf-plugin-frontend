@extends('inc.layout')

@section('content')
    <section class="flat-row flat-slider style4" id="blobgallery">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="slider owl-carousel-15" style="background: transparent;">
                        @foreach($gallery as $slide)
                            <div class="slider-item style8 p-0 p-md-5 mt-5 mb-0"
                                 style="border-radius:0;background: url({{ config('httpmedia'). '/' .  $slide->meta('background') }}), transparent; background-size: cover;">
                                <div class="item-text" style="padding:0; margin:20px 0 0 0;">
                                    <div class="header-item">
                                        {{--<p>Enhanced Technology</p>--}}
                                        <h2 class="name"
                                            style="<?= ($slide->meta('theme') == 'light') ? 'color:#fff;' : '' ?>">{{$slide->meta('slidetitle')}}</h2>
                                        <div style="max-width: 500px; <?= ($slide->meta('theme') == 'light') ? 'color:#fff;' : '' ?>"> {!! $slide->meta('content') !!}</div>
                                    </div>
                                    <div class="content-item">
                                        <div class="price">
                                            <span class="sale"
                                                  style="<?= ($slide->meta('theme') == 'light') ? 'color:#fff;' : '' ?>">{{$slide->meta('prezzo')}}</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        {{--<div class="regular">--}}
                                        {{--{{$slide->meta('prezzo')}}--}}
                                        {{--</div>--}}
                                        <span class="btn-shop">
											<a href="{{$slide->meta('link')}}" title="">{{$slide->meta('call')}}<img
                                                        src="{{asset('assets/images/')}}icons/right-3.png" alt=""></a>
										</span>
                                    </div>
                                </div>
                                @if($slide->meta('image'))
                                    <div class="item-image d-md-flex align-middle d-none"
                                         style="margin:0;padding:0;width:400px; height: 400px;">
                                        <img src="{{the_media($slide->meta('image'))}}">
                                    </div>
                                    <div class="clearfix"></div>
                                @endif
                            </div><!-- /.slider-item style8 -->
                        @endforeach

                    </div><!-- /.slider owl-carousel-15 -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-slider -->

    <section class="flat-row flat-banner-box">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8">
                    <div class="banner-box one-half">
                        <div class="inner-box">
                            <a href="{{$bannerHome->meta('link_banner1')}}" title="">
                                <img src="{{the_media($bannerHome->meta('banner1'))}}" alt="">
                            </a>
                        </div><!-- /.inner-box -->
                        <div class="inner-box">
                            <a href="{{$bannerHome->meta('link_banner2')}}" title="">
                                <img src="{{the_media($bannerHome->meta('banner2'))}}" alt="">
                            </a>
                        </div><!-- /.inner-box -->
                        <div class="clearfix"></div>
                    </div><!-- /.box -->
                    <div class="banner-box one-half">
                        <div class="inner-box">
                            <a href="{{$bannerHome->meta('link_banner4')}}" title="">
                                <img src="{{the_media($bannerHome->meta('banner4'))}}" alt="">
                            </a>
                        </div><!-- /.inner-box -->
                        <div class="inner-box">
                            <a href="{{$bannerHome->meta('link_banner5')}}" title="">
                                <img src="{{the_media($bannerHome->meta('banner5'))}}" alt="">
                            </a>
                        </div><!-- /.inner-box -->
                        <div class="clearfix"></div>
                    </div><!-- /.box -->
                </div><!-- /.col-md-8 -->
                <div class="col-12 col-md-4">
                    <div class="banner-box">
                        <div class="inner-box">
                            <a href="{{$bannerHome->meta('link_banner3')}}" title="">
                                <img src="{{the_media($bannerHome->meta('banner3'))}}" alt="">
                            </a>
                        </div><!-- /.inner-box -->
                    </div><!-- /.box -->
                </div><!-- /.col-md-4 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
        <div class="divider14">
        </div>
    </section><!-- /.flat-banner-box -->

    <section class="flat-imagebox style2 background picked-products">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-wrap p-5">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="flat-row-title">
                                    <h3>Scelti per voi</h3>
                                </div>
                            </div><!-- /.col-md-12 -->
                            @php
                                $posizione= 1;
                            @endphp
                            @foreach($picked as $product)
                                @if($posizione==1)
                                    <div class="col-md-6 first-picked">
                                        @elseif($posizione == 2 || $posizione==4)
                                            <div class="col-md-3">
                                                @endif
                                                <div class="product-box">
                                                    <div class="imagebox style2">
                                                        <div class="status-product">
                                                            @if ($product->state_product == 1)
                                                                <div class="usato">
                                                                    Usato @if ($product->meta('usato_garantito') == 1)
                                                                        Garantito @endif</div>
                                                            @endif


                                                            @if ($product->offer == 1)
                                                                <div class="offerlabel">Offerta</div>
                                                            @endif
                                                        </div>

                                                        <div class="grade-product @if($posizione != 1) small @else fixevidence @endif">
                                                            @if ($product->grade == 'A' || $product->grade == 'B' || $product->grade == 'AB')
                                                                <div class="gradeimg">
                                                                    <img src="{{asset('assets/images/')}}icons/grado-{{strtolower($product->grade)}}.png">
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="mac-powerup-product @if($posizione != 1) small @else fixevidence @endif">
                                                            @if ($product->mac_powerup == 1)
                                                                <div class="gradeimg">
                                                                    <img src="{{asset('assets/images/')}}icons/mac-potenziato.png">
                                                                </div>
                                                            @endif
                                                        </div>

                                                        <div class="box-image">
                                                            <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                               title="">
                                                                <img src="{{ config('httpmedia'). 'ecommerce/prodotti/' . $product->meta('imghighlight') }}"
                                                                     alt="">
                                                            </a>
                                                        </div><!-- /.box-image -->
                                                        <div class="box-content">
                                                            <div class="cat-name">
                                                                <a title="">{{$product->category->title}} </a>
                                                            </div>
                                                            <div class="product-name">
                                                                <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                                   title="">{{$product->title}}</a>
                                                            </div>
                                                            <div class="price">
                                                                @if($product->price_offer > 0)
                                                                    <span class="sale">{{$product->get_price_offer()}} &euro;</span>
                                                                    <span class="regular">{{$product->get_price()}} &euro;</span>
                                                                @else
                                                                    <span class="sale">{{$product->get_price()}} &euro;</span>
                                                                @endif
                                                            </div>
                                                        </div><!-- /.box-content -->
                                                        <div class="box-bottom">
                                                            <div class="btn-add-cart">
                                                                <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                                   title="">
                                                                    <img src="{{asset('assets/images/')}}icons/add-cart.png"
                                                                         alt="">Vai al Prodotto
                                                                </a>
                                                            </div>
                                                            @if(user_logged())
                                                            <div class="compare-wishlist">
                                                                <!--<a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                                   class="compare" title="">
                                                                    <img src="{{asset('assets/images/')}}icons/compare.png"
                                                                         alt="">Compara
                                                                </a>-->
                                                                <a href="#" class="wishlist link-wishlist wish_{{$product->id}}" title="" data-idwish="{{$product->id}}">
                                                                    <i class="fa fa-heart"> </i> <span>Lista dei desideri</span>
                                                                </a>
                                                            </div>
                                                            @endif
                                                        </div><!-- /.box-bottom -->
                                                    </div><!-- /.imagebox style2 -->
                                                </div><!-- /.product-box -->
                                                @if($posizione == 1 || $posizione == 3 || $posizione == 5)
                                            </div>
                                        @endif
                                        @php
                                            $posizione++;
                                        @endphp
                                        @endforeach
                                        @if($posizione == 3 || $posizione  == 5)
                                    </div>
                                @endif
                        </div>
                    </div><!-- /.tab-item -->
                </div><!-- /.product-wrap -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-imagebox style2 -->

    @if(count($bestSeller) > 0)
        <section class="flat-imagebox style4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="flat-row-title">
                            <h3>Best Seller</h3>
                        </div>
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="owl-carousel-3">
                            @foreach($bestSeller as $product)

                                <div class="imagebox style4">
                                    <div class="status-product">
                                        @if ($product->state_product == 1)
                                            <div class="usato">Usato @if($product->meta('usato_garantito') == 1)
                                                    Garantito @endif</div>
                                        @endif

                                        @if ($product->offer == 1)
                                            <div class="offerlabel">Offerta</div>
                                        @endif
                                    </div>

                                    <div class="box-image">
                                        <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                           title="">
                                            <img src="{{ config('httpmedia'). 'ecommerce/prodotti/' . basename($product->meta('imghighlight')) }}"
                                                 alt="">
                                        </a>
                                    </div><!-- /.box-image -->
                                    <div class="box-content">
                                        <div class="cat-name">
                                            <a title="">{{$product->category->title}}</a>
                                        </div>
                                        <div class="product-name">
                                            <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                               title="">{{$product->title}}</a>
                                        </div>
                                        <div class="price">
                                            @if($product->price_offer > 0)
                                                <span class="sale">{{$product->get_price_offer()}} &euro;</span>
                                                <span class="regular">{{$product->get_price()}} &euro;</span>
                                            @else
                                                <span class="sale">{{$product->get_price()}} &euro;</span>
                                            @endif
                                                @if(user_logged())
                                                    <div class="mt-3">
                                                        <a href="#" class="wishlist link-wishlist wish_{{$product->id}}" title="" data-idwish="{{$product->id}}">
                                                            <i class="fa fa-heart"> </i> <span>Lista dei desideri</span>
                                                        </a>
                                                    </div>
                                                @endif
                                        </div>
                                    </div><!-- /.box-content -->
                                </div><!-- /.imagebox style4 -->
                            @endforeach


                        </div><!-- /.owl-carousel-3 -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-imagebox style4 -->
    @endif

    @if(count($productOffer)>0)
        <section class="flat-imagebox style3">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="owl-carousel-2">
                            @foreach($productOffer as $product)
                                <div class="box-counter">
                                    <div class="counter">
                                        <span class="special">Da non perdere</span>
                                        <div class="counter-content">
                                            <p>Approfitta delle offerte speciali</p>
                                            <div class="btn-add-cart">
                                                <a href="/offerte"
                                                   title="" class="off-button">
                                                    <img src="{{asset('assets/images/')}}icons/icona-offerte.svg" style="width:30px; color:#fff; margin-right:10px;"> Visualizza tutte le offerte
                                                </a>
                                            </div>
                                        </div><!-- /.counter-content -->
                                    </div><!-- /.counter -->
                                    <div class="product-item">
                                        <div class="imagebox style3">
                                            <div class="status-product">
                                                @if ($product->state_product == 1)
                                                    <div class="usato">
                                                        Usato @if ($product->meta('usato_garantito') == 1)
                                                            Garantito @endif</div>
                                                @endif
                                            </div>
                                            <div class="box-image save">
                                                <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}" title="">
                                                    <img src="{{ config('httpmedia'). 'ecommerce/prodotti/' . basename($product->meta('imghighlight')) }}"
                                                         style="height:280px!important; margin-top:40px!important; width:auto!important;"
                                                         alt="">
                                                </a>
                                                <span>Risparmia {{number_format($product->price-$product->price_offer,2,',','.')}} &euro;</span>
                                            </div><!-- /.box-image -->
                                            <div class="box-content">
                                                <div class="product-name">
                                                    <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}" title="">{{$product->title}}</a>
                                                </div>
                                                <ul class="product-info">
                                                    {!! $product->description !!}
                                                </ul>
                                                <div class="price">
                                                    @if($product->price_offer > 0)
                                                        <span class="sale">{{$product->get_price_offer()}} &euro;</span>
                                                        <span class="regular">{{$product->get_price()}} &euro;</span>
                                                    @else
                                                        <span class="sale">{{$product->get_price()}} &euro;</span>
                                                    @endif
                                                </div>
                                            </div><!-- /.box-content -->
                                            <div class="box-bottom">
                                                <div class="btn-add-cart">
                                                    <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                       title="">
                                                        <img src="{{asset('assets/images/')}}icons/add-cart.png" alt="">Vai
                                                        al prodotto
                                                    </a>
                                                </div>
                                                @if(user_logged())
                                                    <div class="compare-wishlist">
                                                        <a href="#" class="wishlist link-wishlist wish_{{$product->id}}" title="" data-idwish="{{$product->id}}">
                                                            <i class="fa fa-heart"> </i> <span>Lista dei desideri</span>
                                                        </a>
                                                    </div>
                                                @endif
                                            </div><!-- /.box-bottom -->
                                        </div><!-- /.imagbox style3 -->
                                    </div><!-- /.product-item -->
                                </div><!-- /.box-counter -->
                            @endforeach
                        </div><!-- /.owl-carousel-2 -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-imagebox style3 -->
    @endif

    @if($hero)
        <section id="hero-section"
                 style="margin:10px 0 50px 0; padding:60px 0;background: url({{ config('httpmedia'). '/' . $hero->meta('image') }}); background-size: cover;">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        {!! $hero->meta('content') !!}
                    </div>
                </div>
            </div>
        </section>
    @endif

    {{--<section class="flat-imagebox">--}}
    {{--<div class="container">--}}
    {{--<div class="flat-row-title">--}}
    {{--<h3>Novit&agrave;</h3>--}}
    {{--</div>--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
    {{--<div class="product-tab">--}}
    {{--<ul class="tab-list">--}}
    {{--@if(count($category) > 0)--}}
    {{--@foreach($category as $cat)--}}
    {{--<li>{{$cat->title}}</li>--}}
    {{--@endforeach--}}
    {{--@endif--}}
    {{--</ul>--}}
    {{--</div><!-- /.product-tab -->--}}
    {{--</div><!-- /.col-md-12 -->--}}
    {{--</div><!-- /.row -->--}}
    {{--<div class="box-product">--}}
    {{--@if(count($category) > 0)--}}
    {{--@foreach($category as $cat)--}}
    {{--<div class="row">--}}
    {{--@if(count($productNovelty[$cat->id])>0)--}}

    {{--@php--}}
    {{--$posizione= 0;--}}
    {{--@endphp--}}
    {{--@foreach($productNovelty[$cat->id] as $product)--}}
    {{--@if($posizione%2==0)--}}
    {{--<div class="col-sm-6 col-lg-3">--}}
    {{--@endif--}}
    {{--<div class="product-box">--}}

    {{--<div class="imagebox">--}}
    {{--@if($product->meta('imghighlight'))--}}
    {{--<ul class="box-image">--}}
    {{--<li>--}}
    {{--<a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"--}}
    {{--title="">--}}
    {{--<img src="{{ config('httpmedia'). 'ecommerce/prodotti/' . basename($product->meta('imghighlight')) }}"--}}
    {{--alt="">--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--</ul><!-- /.box-image -->--}}
    {{--@endif--}}
    {{--<div class="box-content">--}}
    {{--<div class="cat-name">--}}
    {{--<a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"--}}
    {{--title="">{{$product->category->title}}</a>--}}
    {{--</div>--}}
    {{--<div class="product-name">--}}
    {{--<a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"--}}
    {{--title="">{{$product->title}}</a>--}}
    {{--</div>--}}
    {{--<div class="price">--}}
    {{--@if($product->price_offer > 0)--}}
    {{--<span class="sale">{{$product->get_price_offer()}} &euro;</span>--}}
    {{--<span class="regular">{{$product->price()}} &euro;</span>--}}
    {{--@else--}}
    {{--<span class="sale">{{$product->price()}} &euro;</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div><!-- /.box-content -->--}}
    {{--<div class="box-bottom">--}}
    {{--<div class="btn-add-cart">--}}
    {{--<a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"--}}
    {{--title="">--}}
    {{--<img src="{{asset('assets/images/')}}icons/add-cart.png"--}}
    {{--alt="">Vai al prodotto--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--<div class="compare-wishlist">--}}
    {{--<a href="#" class="compare" title="">--}}
    {{--<img src="{{asset('assets/images/')}}icons/compare.png"--}}
    {{--alt="">Compare--}}
    {{--</a>--}}
    {{--<a href="#" class="wishlist" title="">--}}
    {{--<img src="{{asset('assets/images/')}}icons/wishlist.png"--}}
    {{--alt="">Wishlist--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--</div><!-- /.box-bottom -->--}}
    {{--</div><!-- /.imagebox -->--}}
    {{--</div><!-- /.product-box -->--}}
    {{--@if($posizione%2==1)--}}
    {{--</div>--}}
    {{--@endif--}}
    {{--@php--}}
    {{--$posizione++;--}}
    {{--@endphp--}}
    {{--@endforeach--}}
    {{--@if($posizione>0 && $posizione%2==1)--}}
    {{--</div>--}}
    {{--@endif--}}
    {{--@php--}}
    {{--$posizione= 0;--}}
    {{--@endphp--}}

    {{--@endif--}}
    {{--</div>--}}
    {{--@endforeach--}}
    {{--@endif--}}
    {{--</div><!-- /.box-product -->--}}
    {{--</div><!-- /.container -->--}}
    {{--</section><!-- /.flat-imagebox -->--}}

    <section class="flat-brand">
        <div class="container">
            <div class="flat-row-title">
                <h3>I nostri marchi </h3>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(count($brands) > 0)

                        <ul class="owl-carousel-5">
                            @foreach($brands as $brand)
                                <li>
                                    <img src="{{ config('httpmedia'). 'ecommerce/brand/' . basename($brand->meta('imghighlight')) }}"
                                         alt="">
                                </li>
                            @endforeach
                        </ul><!-- /.owl-carousel-5 -->
                    @endif
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-brand -->

    <div class="divider30"></div>
@endsection