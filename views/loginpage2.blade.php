@extends('inc.layout')

@section('content')

    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <li class="trail-item">
                            <a href="#" title="">Home</a>
                            <span><img src="{{asset('assets/images/')}}icons/arrow-right.png" alt=""></span>
                        </li>
                        <li class="trail-end">
                            <a href="#" title="">Login</a>
                        </li>
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->


    <section class="flat-account background">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-login">
                        <div class="title">
                            <h3>Login</h3>
                        </div>
                        @if(isset($error) && $error)
                            <h4 class="text-danger"><strong>Errore nel Login!</strong><br>Controlla le credenziali.</h4>
                        @endif

                        <form method="POST" name="login" action="{{path_for('login')}}">
                            <input type="email" name="email" required placeholder="Email" class="form-control">
                            <input type="password" name="password" required placeholder="Password"
                                   class="form-control mt-3">
                            <hr/>
                            <button type="submit" class="btn btn-default mr-3">Accedi</button>
                        </form>
                    </div><!-- /.form-login -->
                </div><!-- /.col-md-6 -->
                <div class="col-md-6">
                    <div class="form-register">
                        <div class="title">
                            <h3>Registati</h3>
                        </div>
                        @if(isset($error_register) && $error_register)
                            <h4 class="text-danger"><strong>Errore nella registrazione!</strong>{{$error_register}}</h4>
                        @endif

                        <form method="POST" name="login" action="{{path_for('register')}}">
                            <input type="email" name="customer[email]" required placeholder="Email" class="form-control">
                            <input type="password" name="customer[password]" required placeholder="Password"
                                   class="form-control mt-3">
                            <hr/>
                            <button type="submit" class="btn btn-default mr-3">Registrati</button>
                        </form>
                    </div><!-- /.form-register -->
                </div><!-- /.col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-account -->
@endsection