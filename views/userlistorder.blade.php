@extends('inc.layout')

@section('content')

    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <li class="trail-item">
                            <a href="#" title="">Home</a>
                            <span><img src="{{asset('assets/images/')}}icons/arrow-right.png" alt=""></span>
                        </li>
                        <li class="trail-end">
                            <a href="#" title="">Area User</a>
                        </li>
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->


    <section class="flat-account background">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    @include('inc.userbar')
                </div><!-- /.col-md-4 -->

                <div class="col-md-9">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                                <h2>
                                    @if($table && count($table)>0)
                                        <strong>I Tuoi Ordini</strong>
                                    @else
                                        <strong>Non sono presenti ordini nella tua area utente</strong>
                                    @endif
                                </h2>
                                @if($table && count($table)>0)

                                    <table id="tabella" class="table m-t-30 table-hover contact-list">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>titolo</th>
                                            <th>Data</th>
                                            <th>Stato</th>
                                            <th>Totale</th>
                                            <th>Azioni</th>
                                        </tr>
                                        </thead>

                                        <tfoot>

                                        </tfoot>

                                        <tbody>
                                        @foreach ($table as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>
                                                <td>{{ $row->title }}</td>
                                                <td>{{date('d/m/Y', strtotime($row->date))}}</td>
                                                <td>
                                                    @if ($row->status == 0)
                                                        <span class="label label-warning">In attesa di pagamento</span>
                                                    @elseif ($row->status == 1)
                                                        <span class="label label-default">In lavorazione</span>
                                                    @elseif ($row->status == 2)
                                                        <span class="label label-success">Spedito</span>
                                                    @elseif ($row->status == 4)
                                                        <span class="label label-success">Annullato</span>
                                                    @elseif ($row->status == 5)
                                                        <span class="label label-success">Rimborsato</span>
                                                    @elseif ($row->status == 6)
                                                        <span class="label label-success">Ritirato in sede</span>
                                                    @else
                                                        <span class="label label-warning">in lavorazione</span>
                                                    @endif
                                                </td>
                                                <td>{{ $row->total }} &euro;</td>
                                                <td>
                                                    <div class="btn-group" role="group" aria-label="...">
                                                        <a href=" {{ path_for('user-info-order', ['id_order' => $row->id]) }}" class="btn btn-sm btn-default"><span title="dettaglio ordine" class='fa  fa-eye'></span></a>
                                                        @if (($row->status == 0) && ($row->payment_type==2))
                                                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                                            <input type="hidden" name="business" value="paypal@blobvideo.com">
                                                            <input type="hidden" name="return" value="{{ path_for('single-page', ['permalink' => 'success-checkout-paypal']) }}" />
                                                            <input type="hidden" name="cancel_return" value="{{ path_for('single-page', ['permalink' => 'error-checkout']) }}" />
                                                            <input type="hidden" name="notify_url" value="{{ path_for('pagamento-paypal') }}" />
                                                            <input type="hidden" name="cmd" value="_xclick">
                                                            <input type="hidden" name="currency_code" value="EUR">
                                                            <input type="hidden" id="titoloPaypal" name="item_name" value="Ordine n {{$row->id}} ">
                                                            <input type="hidden" id="costoPaypal" name="amount"  value="{{$row->total}}">
                                                            <input type="hidden" name="custom" id="ordineid" value="{{$row->id}}" >
                                                            <input type="submit" border="0" id="submitPaypal" name="submit" value="Paga con PayPal">
                                                        </form>
                                                            @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endif

                            </div>
                        </div>
                    </div>
                </div><!-- /.col-md-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-account -->
@endsection



@section('scripts')

    <script>
        $(function () {
            $("#form-save").validate({
                submitHandler: function(form) {
                    $('#result').removeClass('btn-success btn-danger').addClass('btn btn-default').html("Operazione in corso");
                    $('#' + form.id).ajaxSubmit({
                        success: showResponse,
                        dataType: "json"
                    });
                    return false;
                },
                errorClass: "help-block",
                errorElement: "div",
                errorPlacement: function(e, t) {
                    t.parents(".col-form").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".col-form").removeClass("has-success has-error").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".col-form").removeClass("has-success has-error").addClass("has-success"), e.closest(".help-block").remove();
                }
            });

        });

        function showResponse(responseText, statusText, xhr, $form) {
            var res = responseText.result;
            var order;
            if (!isNaN(res)) {
                $("#result").removeClass("btn btn-error").addClass("btn btn-success").html("Salvataggio eseguito con successo");
            } else {
                $("#result").removeClass("btn btn-success").addClass("btn btn-error").html("Attenzione: si è verificato un errore");
            }
        }

    </script>
@endsection