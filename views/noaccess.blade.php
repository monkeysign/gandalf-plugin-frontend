@extends('inc.layout')

@section('content')
    <div class="row">
        <div class="col text-center">
            <h1>Accesso Negato</h1>
            <p>Per visualizzare questa risorsa devi essere loggato</p>
        </div>
    </div>
@endsection