@extends('inc.layout')

@section('content')

    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <li class="trail-item">
                            <a href="#" title="">Home</a>
                            <span><img src="{{asset('assets/images/')}}icons/arrow-right.png" alt=""></span>
                        </li>
                        <li class="trail-end">
                            <a href="#" title="">Area User</a>
                        </li>
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->


    <section class="flat-account background">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    @include('inc.userbar')
                </div><!-- /.col-md-4 -->

                <div class="col-md-9">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                                <h2><strong>Dettagli Ordine</strong></h2>
                                <form action="#" method="POST"
                                      enctype="multipart/form-data">
                                    <input id="item_id_hidden" type="hidden"
                                           value='@if($record->id){{ $record->id }}@else{{0}}@endif' name='item[id]'>
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label title="Campo obbligatorio"
                                                           class="control-label">Id</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-user"></i>
                                                        </div>
                                                        <input value="{{$record->id}}" data-toggle="validator" disabled type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <label title="Campo obbligatorio"
                                                           class="control-label">Titolo</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-user"></i>
                                                        </div>
                                                        <input value="{{$record->title}}" disabled="" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label title="Campo obbligatorio"
                                                           class="control-label">Data Ordine</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-user"></i>
                                                        </div>
                                                        <input value="{{date('d/m/Y', strtotime($record->date))}}" disabled="" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                        @if($record->orderProduct)
                                            <div class="m-3">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <strong>Prodotto</strong>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <strong>Codice Prodotto</strong>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <strong>Stato</strong>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <strong>Quantit&agrave;</strong>
                                                    </div>
                                                </div>
                                                @foreach($record->orderProduct as $prodotto)
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            {{$prodotto->title}}
                                                        </div>
                                                        <div class="col-md-2">
                                                            <a target="u_blank" href="{{path_for('product', ['permalink' => $prodotto->product->getPermalink()])}}">
                                                            {{$prodotto->product->permalink}}
                                                            </a>
                                                        </div>
                                                        <div class="col-md-2">
                                                            @if($prodotto->product->state_product==1)
                                                                Usato
                                                            @else
                                                                Nuovo
                                                            @endif
                                                        </div>
                                                        <div class="col-md-2">
                                                            {{$prodotto->quantity}}
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endif

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label
                                                            title="Stato Ordine"
                                                            class="control-label">Stato Ordine</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-light-bulb"></i>
                                                        </div>
                                                        <select name="item[status]" disabled class="form-control select-choose">
                                                            <option
                                                                    @if ( $record->status == 0)
                                                                    selected @endif
                                                                    value="0">In attesa di pagamento
                                                            </option>
                                                            <option
                                                                    @if ( $record->status == 1)
                                                                    selected @endif
                                                                    value="1">In Lavorazione
                                                            </option>
                                                            <option
                                                                    @if ( $record->status == 2)
                                                                    selected @endif
                                                                    value="2">Spedito
                                                            </option>
                                                            <option
                                                                    @if ( $record->status == 4)
                                                                    selected @endif
                                                                    value="4">Annullato
                                                            </option>
                                                            <option
                                                                    @if ( $record->status == 5)
                                                                    selected @endif
                                                                    value="5">Rimborsato
                                                            </option>
                                                            <option
                                                                    @if ( $record->status == 6)
                                                                    selected @endif
                                                                    value="6">Ritirato in sede
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <span class="help-block"> </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label title="Campo obbligatorio"
                                                           class="control-label">Metodo Pagamento</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-user"></i>
                                                        </div>
                                                        <input value="{{$record->typePayment->title}}" disabled  data-toggle="validator" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>




                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Tracking Code</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-user"></i>
                                                        </div>
                                                        <input value="{{$record->tracking}}" name="item[tracking]" disabled data-toggle="validator" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Contenuto Scaricabile</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-user"></i>
                                                        </div>
                                                        <input value="{{$record->downloadable}}" name="item[downloadable]" disabled data-toggle="validator" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Spedizione</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-user"></i>
                                                        </div>
                                                        <input value="{{$record->typeShip->title}}" name="item[spedizione]" disabled data-toggle="validator" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label title="Campo obbligatorio"
                                                           class="control-label">Totale</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-user"></i>
                                                        </div>
                                                        <input value="{{$record->total_label}}" disabled data-toggle="validator" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Stato Pagamento</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-light-bulb"></i>
                                                        </div>
                                                        <select name="item[payment_status]" disabled class="form-control select-choose">
                                                            <option
                                                                    @if ( $record->payment_status == 0)
                                                                    selected @endif
                                                                    value="0">Non Pagato
                                                            </option>
                                                            <option
                                                                    @if ( $record->payment_status == 1)
                                                                    selected @endif
                                                                    value="1">Pagato
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <span class="help-block"> </span>
                                                </div>
                                            </div>
                                            <!--
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label title="Campo obbligatorio"
                                                           class="control-label">Data Pagamento</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-user"></i>
                                                        </div>
                                                        <input value="{{$record->payment_date}}" name="item[payment_date]" disabled data-toggle="validator" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div> -->

                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Note</label>
                                                    <div class="input-group">
                                                        <textarea disabled style="width: 100%">{{$record->note}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        @include('address_order')
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div><!-- /.col-md-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-account -->
@endsection



@section('scripts')

    <script>
        $(function () {
            $("#form-save").validate({
                submitHandler: function(form) {
                    $('#result').removeClass('btn-success btn-danger').addClass('btn btn-default').html("Operazione in corso");
                    $('#' + form.id).ajaxSubmit({
                        success: showResponse,
                        dataType: "json"
                    });
                    return false;
                },
                errorClass: "help-block",
                errorElement: "div",
                errorPlacement: function(e, t) {
                    t.parents(".col-form").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".col-form").removeClass("has-success has-error").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".col-form").removeClass("has-success has-error").addClass("has-success"), e.closest(".help-block").remove();
                }
            });

        });

        function showResponse(responseText, statusText, xhr, $form) {
            var res = responseText.result;
            var order;
            if (!isNaN(res)) {
                $("#result").removeClass("btn btn-error").addClass("btn btn-success").html("Salvataggio eseguito con successo");
            } else {
                $("#result").removeClass("btn btn-success").addClass("btn btn-error").html("Attenzione: si è verificato un errore");
            }
        }

    </script>
@endsection