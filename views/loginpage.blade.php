@extends('inc.layout')

@section('content')

    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <li class="trail-item">
                            <a href="#" title="">Home</a>
                            <span><img src="{{asset('assets/images/')}}icons/arrow-right.png" alt=""></span>
                        </li>
                        <li class="trail-end">
                            <a href="#" title="">Login</a>
                        </li>
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->


    <section class="flat-account background">
        <div class="container">
            <div class="row">
                <div class="col-md-7">

                    <div class="form-login">
                        <div class="title">
                            <h3>Accedi al tuo account</h3>
                            <hr>
                        </div>

                        @if(isset($error) && $error)
                            <div>
                                <h4 class="text-danger"><strong>Errore nel Login: </strong>Controlla le credenziali.</h4>
                                <br>
                            </div>

                        @endif

                        {{--<div class="row mb-5">--}}
                            {{--<div class="col-md-6">--}}
                                {{--<a class="btn btn-default" href="{{($urlfb)}}" style="background: #4267B2; color: #fff;">Accedi con Facebook</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}


                        {{--<div class="title">--}}
                            {{--<h3>Oppure inserisci i tuoi dati</h3>--}}
                            {{--<hr>--}}
                        {{--</div>--}}

                        <form method="POST" name="login" action="{{path_for('login')}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Email</h3>
                                    <input type="email" name="email" required placeholder="Email" class="form-control mt-3">
                                </div>
                                <div class="col-md-6">
                                    <h3>Password</h3>
                                    <input type="password" name="password" required placeholder="Password"
                                           class="form-control mt-3">
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-12 text-center">
                                    <label class="pull-right"><a href="#" onclick="$('#RecuperaPassword').toggle();$('html, body').animate({scrollTop: $('#RecuperaPassword').height()-10}, 1000);
                                        return false;">Hai dimenticato la password?</a></label>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn mr-3 default-color"> Accedi </button>
                                </div>
                            </div>
                        </form>
                        <form id="RecuperaPassword" style="display: none" method="POST" name="recuperapass" action="{{path_for('recuperapass')}}">
                            <hr>
                            <h3>Recupera Password</h3>
                            <p class="text-left">Inserisci la tua email nel form sottostante, ti invieremo una mail con le dovute istruzioni.</p>
                            <br/>
                            <div class="form-group col-form">
                                <label>Email <sup>*</sup></label>
                                <input type="email" id="emailRecupera" required name="emailRecupera" placeholder="Email" class="form-control" />
                            </div>
                            <div class="col-form text-center"  >
                                <div class="response-recupera"> </div>
                                <button type="submit" name="submitRecupera" id="recupera-psw" class="btn mr-3 default-color"> Recupera Password</button>
                            </div>

                        </form>
                    </div><!-- /.form-login -->
                </div><!-- /.col-md-8 -->
                <div class="col-md-5 right-form-login ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Sei un nuovo cliente? Registrati gratis</h3>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-checkbox style2">
                                <div class="checkbox">
                                        <input type="checkbox" name="category" checked>
                                        <label for="gionee">Crea e gestisci la tua lista dei desideri</label>
                                    </div>
                                <div class="checkbox">
                                    <input type="checkbox"  name="category" checked>
                                    <label for="gionee">Ricevi avvisi quando un prodotto torna disponibile</label>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox"  name="category" checked>
                                    <label for="gionee">Velocizza gli acquisti con le tue preferenze di spedizione</label>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox" name="category" checked>
                                    <label for="gionee">Tieni traccia dei tuoi ordini</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center mt-3">
                            <a href="{{ path_for('registrationp', array()) }}">
                                <button type="submit" class="btn  default-color"> Registrati </button>
                            </a>

                        </div>
                    </div>
                </div><!-- /.col-md-4 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-account -->

    <section class="flat-row flat-iconbox style3">
        <div class="container">
            <div class="row">
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/banconota.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Spedizioni Gratis</h4>
                                <p>per ordini superiori a 79&euro;</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/consegna.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Consegne 24/48 H</h4>
                                <p>con corriere espresso</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img style="width:45px;" src="{{asset('assets/images/')}}icons/pagamentisicuri.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Pagamenti Sicuri</h4>
                                <p>con PayPal e Bonifico</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/utente.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Registrati al Sito</h4>
                                <p>per Offerte e Promozioni</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/fumetto.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Contattaci</h4>
                                <p>per maggiori informazioni</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-iconbox -->
@endsection




@section('scripts')
    <script>
        $(function () {
            $("#RecuperaPassword").validate({
                submitHandler: function(form) {
                    $('.response-recupera').removeClass('btn-success btn-danger').addClass('btn btn-default').html("Operazione in corso");
                    $('#' + form.id).ajaxSubmit({
                        dataType: "json",
                        success: showResponsePass
                    });
                    return false;
                },
                errorClass: "help-block",
                errorElement: "div",
                errorPlacement: function(e, t) {
                    t.parents(".col-form").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".col-form").removeClass("has-success has-error").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".col-form").removeClass("has-success has-error").addClass("has-success"), e.closest(".help-block").remove();
                },
                rules: {
                    emailRecupera: {required: !0, email: !0}

                },
                messages: {
                    emailRecupera: "Emamil non valida"
                }
            });


        });

        function showResponsePass(responseText, statusText, xhr, $form) {
            var res = responseText.result;
            if (!isNaN(res)) {
                $('#recupera-psw').remove();
                $('.response-recupera').addClass('btn btn-success').html("Ti abbiamo inviato una email con le istruzioni da eseguire");
            } else {
                $('.response-recupera').addClass('btn btn-danger').html(res);

            }
        }

    </script>
@endsection