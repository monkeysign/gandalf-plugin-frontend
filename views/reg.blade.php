@extends('inc.layout')

@section('content')

    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <li class="trail-item">
                            <a href="#" title="">Home</a>
                            <span><img src="{{asset('assets/images/')}}icons/arrow-right.png" alt=""></span>
                        </li>
                        <li class="trail-end">
                            <a href="#" title="">Registrazione</a>
                        </li>
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->


    <section class="flat-account background">
        <div class="container">
            <div class="row">
                <div class="col-md-7">

                    <div class="form-login">
                        <div class="title">
                            <h3>Inserisci i dati dell'account</h3>
                            <hr>
                        </div>

                        @if(isset($error) && $error)
                            <div>
                                <h4 class="text-danger"><strong>Errore nel Login: </strong>Controlla le credenziali.</h4>
                                <br>
                            </div>

                        @endif

                        <form method="POST" name="registrazione" action="{{path_for('registrazione')}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Email</h3>
                                    <input type="text" name="name" required placeholder="Nome" class="form-control mt-3">
                                </div>
                                <div class="col-md-6">
                                    <h3>Email</h3>
                                    <input type="text" name="surname" required placeholder="Cognome" class="form-control mt-3">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Email</h3>
                                    <input type="email" name="email" required placeholder="Email" class="form-control mt-3">
                                </div>
                                <div class="col-md-6">
                                    <h3>Password</h3>
                                    <input type="password" name="password" required placeholder="Password"
                                           class="form-control mt-3">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn mr-3 default-color"> Accedi </button>
                                </div>
                            </div>
                        </form>
                    </div><!-- /.form-login -->
                </div><!-- /.col-md-8 -->
                <div class="col-md-5 right-form-login ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Sei un nuovo cliente? Registrati gratis</h3>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-checkbox style2">
                                <div class="checkbox">
                                        <input type="checkbox" name="category" checked>
                                        <label for="gionee">Crea e gestisci la tua lista dei desideri</label>
                                    </div>
                                <div class="checkbox">
                                    <input type="checkbox"  name="category" checked>
                                    <label for="gionee">Ricevi avvisi quando un prodotto torna disponibile</label>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox" name="category" checked>
                                    <label for="gionee">Tienitraccia dei tuoi ordini</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center mt-3">
                            <a href="{{ path_for('register', array()) }}">
                                <button type="submit" class="btn  default-color"> Registrati </button>
                            </a>

                        </div>
                    </div>
                </div><!-- /.col-md-4 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-account -->
@endsection