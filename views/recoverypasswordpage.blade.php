@extends('inc.layout')

@section('content')

    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <li class="trail-item">
                            <a href="#" title="">Home</a>
                            <span><img src="{{asset('assets/images/')}}icons/arrow-right.png" alt=""></span>
                        </li>
                        <li class="trail-end">
                            <a href="#" title="">Recovery Passwrod</a>
                        </li>
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->


    <section class="flat-account background">
        <div class="container">
            <div class="row">
                <div class="col-md-7">

                    <div class="form-login" style="height: auto">
                        <div class="title text-left">
                            <h3>Ripristina Password</h3>
                            <p>Procedura per il rispistino password</p>
                            <hr>
                        </div>
                        <form method="POST" name="registrazione" id="recoveryPassword" action="{{path_for('save-password')}}">
                            <input type="hidden" value="{{$id}}" name="customer[id]" />
                            <div class="row mt-3">
                                <div class="col-md-6 col-form">
                                    <h3>Password*</h3>
                                    <input type="password" name="customer[password]" required placeholder="Password" class="form-control mt-1">
                                </div>
                                <div class="col-md-6 col-form">
                                    <h3>Conferma Password*</h3>
                                    <input type="password" name="customer[passwordRepeat]" required placeholder="Ripeti Password" class="form-control mt-1">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-left col-form">
                                    <input type="checkbox"  required name="informativa" style="opacity: 1"/>Ho letto ed accetto <a class="active" target="u_blank" href="{{ path_for('single-page', ['permalink' => 'privacy']) }}">Privacy e Cookie Policy*</a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" id="bottone-recoveryPassword" class="btn mr-3 mt-3 default-color"> Cambia Password </button>
                                    <span class="response-recoveryPassword"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="small">I campi contrassegnati con * sono da considerarsi obbligatori</p>
                                </div>
                            </div>
                        </form>
                    </div><!-- /.form-login -->
                </div><!-- /.col-md-8 -->
                <div class="col-md-5 right-form-login ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Vantaggi riservati agli utenti registrati</h3>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-checkbox style2">
                                <div class="checkbox">
                                    <input type="checkbox" name="category" checked>
                                    <label for="gionee">Crea e gestisci la tua lista dei desideri</label>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox"  name="category" checked>
                                    <label for="gionee">Ricevi avvisi quando un prodotto torna disponibile</label>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox"  name="category" checked>
                                    <label for="gionee">Velocizza gli acquisti con le tue preferenze di spedizione</label>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox" name="category" checked>
                                    <label for="gionee">Tieni traccia dei tuoi ordini</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col-md-4 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-account -->
    <section class="flat-row flat-iconbox style3">
        <div class="container">
            <div class="row">
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/car.png" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Spedizioni Gratis</h4>
                                <p>per ordini superiori a 79&euro;</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/car.png" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Consegne 24/48 H</h4>
                                <p>con corriere espresso</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/payment.png" alt="">
                            </div>
                            <div class="box-title">
                                <h3>Pagamenti Sicuri</h3>
                                <p>con PayPal e Bonifico</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/return.png" alt="">
                            </div>
                            <div class="box-title">
                                <h3>Registrati al Sito</h3>
                                <p>per Offerte e Promozioni</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/order.png" alt="">
                            </div>
                            <div class="box-title">
                                <h3>Contattaci</h3>
                                <p>per maggiori informazioni</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-iconbox -->
@endsection


@section('scripts')
    <script>
        $(function () {
            $("#recoveryPassword").validate({
                submitHandler: function(form) {
                    $('.response-recoveryPassword').removeClass('btn-success btn-danger').addClass('btn btn-primary').html("Operazione in corso");
                    $('#' + form.id).ajaxSubmit({
                        success: showResponseRecoveryPass,
                        dataType: "json"
                    });
                    return false;
                },
                errorClass: "help-block",
                errorElement: "div",
                rules: {
                    "customer[password]": {required: !0},
                    "customer[passwordRepeat]": {required: !0},
                    informativa: {required: !0}
                },
                messages: {
                    "customer[password]": "Questo campo è obbligatorio.",
                    "customer[passwordRepeat]": "Questo campo è obbligatorio.",
                    informativa: "Questo campo è obbligatorio."
                },
                errorPlacement: function(e, t) {
                    t.parents(".col-form").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".col-form").removeClass("has-success has-error ").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".col-form").removeClass("has-success has-error mt-5").addClass("has-success"), e.closest(".help-block").remove();
                }
            });

        });

        function showResponseRecoveryPass(responseText, statusText, xhr, $form) {
            var res = responseText.result;
            if (res==1) {
                $('#bottone-recoveryPassword').remove();
                $('.response-recoveryPassword').removeClass('btn-success btn-danger').addClass('btn btn-success').html("<p>Procedura cambio password avvenuta con successo</p>");
            } else {
                $('.response-recoveryPassword').removeClass('btn btn-success mt-5').addClass('btn btn-danger mt-5').html("<p> "+ res +"</p>");
            }
        }

    </script>
@endsection