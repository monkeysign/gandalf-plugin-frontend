<h4>Profilazione</h4>
<hr>
<div class="row">
    <div class="col-12 col-md-6">
        <div class="form-group">
            <label title="Campo obbligatorio"
                   class="control-label">Ragione Sociale</label>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="ti-briefcase"></i>
                </div>
                <input value="{{$record->meta('business')}}"
                       data-toggle="validator" type="text" name="meta[business]"
                       id="meta_business" class="form-control">
            </div>
            <span class="help-block with-errors"> </span>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="form-group">
            <label title="Campo obbligatorio"
                   class="control-label">P.IVA</label>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="ti-crown"></i>
                </div>
                <input value="{{$record->meta('vat')}}"
                       data-toggle="validator" type="text" name="meta[vat]"
                       id="meta_vat" class="form-control">
            </div>
            <span class="help-block with-errors"> </span>
        </div>
    </div>
</div>

{{ hooks()->do_action(CRM_ADMIN_CUSTOMER_FORM_META, $record) }}