@extends('inc.layout')

@section('content')

    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <li class="trail-item">
                            <a href="#" title="">Home</a>
                            <span><img src="{{asset('assets/images/')}}icons/arrow-right.png" alt=""></span>
                        </li>
                        <li class="trail-end">
                            <a href="#" title="">Area User</a>
                        </li>
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->


    <section class="flat-account background">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    @include('inc.userbar')
                </div><!-- /.col-md-4 -->

                <div class="col-md-9">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                                <h2>
                                    @if($record->id)
                                        <strong>I Tuoi Dati</strong>
                                    @endif
                                </h2>
                                <hr/>
                                <form id="form-save" action="{{path_for('user-save')}}" method="POST"
                                      enctype="multipart/form-data">
                                    <input id="item_id_hidden" type="hidden"
                                           value='@if($record->id){{ $record->id }}@else{{0}}@endif' name='item[id]'>
                                    <div class="form-body">
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label title="Campo obbligatorio"
                                                           class="control-label">Nome *</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-user"></i>
                                                        </div>
                                                        <input value="{{$record->name}}" required
                                                               data-toggle="validator" type="text" name="item[name]"
                                                               id="name" class="form-control"
                                                               placeholder="Nome">
                                                    </div>
                                                    <span class="help-block with-errors"> </span>
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label title="Campo Obbligatorio"
                                                           class="control-label">Cognome*</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-user"></i>
                                                        </div>
                                                        <input value="{{$record->surname}}" required
                                                               data-toggle="validator" type="text" name="item[surname]"
                                                               id="surname" class="form-control"
                                                               placeholder="Cognome">
                                                    </div>
                                                    <span class="help-block with-errors"> </span>
                                                </div>
                                            </div>


                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label title="Campo obbligatorio"
                                                           class="control-label">Telefono Predefinito</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-mobile"></i>
                                                        </div>
                                                        <input value="{{$record->meta('phone')}}"
                                                               data-toggle="validator" type="text" name="meta[phone]"
                                                               id="meta_phone" class="form-control">
                                                    </div>
                                                    <span class="help-block with-errors"> </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label title="Campo Obbligatorio"
                                                           class="control-label">Email*</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-email"></i>
                                                        </div>
                                                        <input value="{{$record->email}}" type="email" required
                                                               name="item[email]" id="email" class="form-control"
                                                               placeholder="Email">
                                                    </div>
                                                    <span class="help-block with-errors"> </span>
                                                </div>
                                            </div>

                                        </div>
                                        <!--/row-->
                                        @include('user.meta_customer')

                                        @include('user.address_customer')
                                    </div>
                                    <!--/row-->

                                    <!--/span-->
                                    <div class="form-actions">
                                        <span id="result"></span>
                                        <hr style="margin-top: 0px">
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-save"></i>
                                            Salva
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col-md-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-account -->
@endsection



@section('scripts')

    <script>
        $(function () {
            $("#form-save").validate({
                submitHandler: function(form) {
                    $('#result').removeClass('btn-success btn-danger').addClass('btn btn-default').html("Operazione in corso");
                    $('#' + form.id).ajaxSubmit({
                        success: showResponse,
                        dataType: "json"
                    });
                    return false;
                },
                errorClass: "help-block",
                errorElement: "div",
                errorPlacement: function(e, t) {
                    t.parents(".col-form").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".col-form").removeClass("has-success has-error").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".col-form").removeClass("has-success has-error").addClass("has-success"), e.closest(".help-block").remove();
                }
            });

        });

        function showResponse(responseText, statusText, xhr, $form) {
            var res = responseText.result;
            var order;
            if (!isNaN(res)) {
                $("#result").removeClass("btn btn-error").addClass("btn btn-success").html("Salvataggio eseguito con successo");
            } else {
                $("#result").removeClass("btn btn-success").addClass("btn btn-error").html("Attenzione: si è verificato un errore");
            }
        }

    </script>
@endsection