@extends('inc.layout')

@section('content')
    <section class="flat-row flat-slider style4" id="blobgallery">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="slider owl-carousel-15">
                        @foreach($gallery as $slide)
                            <div class="slider-item style8">
                                <div class="item-text">
                                    <div class="header-item">
                                        {{--<p>Enhanced Technology</p>--}}
                                        <h2 class="name">{{$slide->meta('title')}}</h2>
                                        <p>
                                            The ship set ground on the shore of this uncharted desert isle <br/>with
                                            Gilligan the Skipper too the millionaire and his story.
                                        </p>
                                    </div>
                                    <div class="content-item">
                                        <div class="price">
                                            <span class="sale">{{$slide->meta('prezzo')}}</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        {{--<div class="regular">--}}
                                        {{--{{$slide->meta('prezzo')}}--}}
                                        {{--</div>--}}
                                        <span class="btn-shop">
											<a href="{{$slide->meta('link')}}" title="">Visualizza ora<img
                                                        src="{{asset('assets/images/')}}icons/right-3.png" alt=""></a>
										</span>
                                    </div>
                                </div>
                                <div class="item-image">
                                    <img src="{{the_media($slide->meta('image'))}}"alt="" style="width:450px;">
                                </div>
                                <div class="clearfix"></div>
                            </div><!-- /.slider-item style8 -->
                        @endforeach

                    </div><!-- /.slider owl-carousel-15 -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-slider -->

    <section class="flat-row flat-banner-box">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="banner-box one-half">
                        <div class="inner-box">
                            <a href="#" title="">
                                <img src="{{asset('assets/images/')}}banner_boxes/home-01.jpg" alt="">
                            </a>
                        </div><!-- /.inner-box -->
                        <div class="inner-box">
                            <a href="#" title="">
                                <img src="{{asset('assets/images/')}}banner_boxes/home-05.jpg" alt="">
                            </a>
                        </div><!-- /.inner-box -->
                        <div class="clearfix"></div>
                    </div><!-- /.box -->
                    <div class="banner-box">
                        <div class="inner-box">
                            <a href="#" title="">
                                <img src="{{asset('assets/images/')}}banner_boxes/home-04.jpg" alt="">
                            </a>
                        </div>
                    </div><!-- /.box -->
                </div><!-- /.col-md-8 -->
                <div class="col-md-4">
                    <div class="banner-box">
                        <div class="inner-box">
                            <a href="#" title="">
                                <img src="{{asset('assets/images/')}}banner_boxes/home-03.jpg" alt="">
                            </a>
                        </div><!-- /.inner-box -->
                    </div><!-- /.box -->
                </div><!-- /.col-md-4 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
        <div class="divider14">
        </div>
    </section><!-- /.flat-banner-box -->

    <section class="flat-imagebox style2 background">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-wrap">
                        <div class="product-tab style1">
                            <ul class="tab-list">
                                @if(count($category) > 0)
                                    @foreach($category as $cat)
                                        <li>{{$cat->title}}</li>
                                    @endforeach
                                @endif
                            </ul><!-- /.tab-list -->
                        </div><!-- /.product-tab style1 -->
                        <div class="tab-item">
                            @if(count($category) > 0)
                                @foreach($category as $cat)
                                    <div class="row">
                                        @if(count($prodottiBox1[$cat->id])>0)
                                            @php
                                                $posizione= 1;
                                            @endphp
                                            @foreach($prodottiBox1[$cat->id] as $product)
                                                @if($posizione==1)
                                                    <div class="col-md-6">
                                                        @elseif($posizione ==2 || $posizione==4)
                                                            <div class="col-md-3">
                                                                @endif
                                                                <div class="product-box">
                                                                    <div class="imagebox style2">
                                                                        <div class="box-image">
                                                                            <a href="#" title="">
                                                                                <img src="{{ config('httpmedia'). 'ecommerce/prodotti/' . $product->meta('imghighlight') }}" alt="">
                                                                            </a>
                                                                        </div><!-- /.box-image -->
                                                                        <div class="box-content">
                                                                            <div class="cat-name">
                                                                                <a href="#" title="">{{$product->category->title}} </a>
                                                                            </div>
                                                                            <div class="product-name">
                                                                                <a href="#" title="">{{$product->title}}</a>
                                                                            </div>
                                                                            <div class="price">
                                                                                @if(($product->offer==1) && ($product->price_offert>0))
                                                                                    <span class="sale">{{$product->price_offer}} &euro;</span>
                                                                                    <span class="regular">{{$product->price}} &euro;</span>
                                                                                @else
                                                                                    <span class="sale">{{$product->price}} &euro;</span>
                                                                                @endif
                                                                            </div>
                                                                        </div><!-- /.box-content -->
                                                                        <div class="box-bottom">
                                                                            <div class="btn-add-cart">
                                                                                <a href="{{ path_for('product', ['permalink' => $product->permalink]) }}" title="">
                                                                                    <img src="{{asset('assets/images/')}}icons/add-cart.png" alt="">Vai al Prodotto
                                                                                </a>
                                                                            </div>
                                                                            <div class="compare-wishlist">
                                                                                <a href="{{ path_for('product', ['permalink' => $product->permalink]) }}" class="compare" title="">
                                                                                    <img src="{{asset('assets/images/')}}icons/compare.png" alt="">Compara
                                                                                </a>
                                                                                <a href="{{ path_for('product', ['permalink' => $product->permalink]) }}" class="wishlist" title="">
                                                                                    <img src="{{asset('assets/images/')}}icons/wishlist.png" alt="">Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div><!-- /.box-bottom -->
                                                                    </div><!-- /.imagebox style2 -->
                                                                </div><!-- /.product-box -->
                                                                @if($posizione ==1 || $posizione ==3 || $posizione==5)
                                                            </div>
                                                        @endif
                                                        @php
                                                            $posizione++;
                                                        @endphp
                                                        @endforeach
                                                        @endif
                                                    </div>
                                                    @endforeach
                                                @endif
                                    </div><!-- /.tab-item -->
                        </div><!-- /.product-wrap -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
    </section><!-- /.flat-imagebox style2 -->


    @if(count($bestSeller) > 0)
        <section class="flat-imagebox style4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="flat-row-title">
                            <h3>Best Seller</h3>
                        </div>
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="owl-carousel-3">
                            @foreach($bestSeller as $product)

                                <div class="imagebox style4">
                                    <div class="box-image">
                                        <a href="{{ path_for('product', ['permalink' => $product->permalink]) }}"
                                           title="">
                                            <img src="{{ config('httpmedia'). 'ecommerce/prodotti/' . basename($product->meta('imghighlight')) }}" alt="">
                                        </a>
                                    </div><!-- /.box-image -->
                                    <div class="box-content">
                                        <div class="cat-name">
                                            <a href="{{ path_for('product', ['permalink' => $product->permalink]) }}"
                                               title="">{{$product->category->title}}</a>
                                        </div>
                                        <div class="product-name">
                                            <a href="{{ path_for('product', ['permalink' => $product->permalink]) }}"
                                               title="">{{$product->title}}</a>
                                        </div>
                                        <div class="price">
                                            @if(($product->offer==1) && ($product->price_offert>0))
                                                <span class="sale">{{$product->price_offer}} &euro;</span>
                                                <span class="regular">{{$product->price}} &euro;</span>
                                            @else
                                                <span class="sale">{{$product->price}} &euro;</span>
                                            @endif
                                        </div>
                                    </div><!-- /.box-content -->
                                </div><!-- /.imagebox style4 -->
                            @endforeach


                        </div><!-- /.owl-carousel-3 -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-imagebox style4 -->
    @endif


    @if(count($productOffer)>0)
        <section class="flat-imagebox style3">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="owl-carousel-2">
                            @foreach($productOffer as $product)
                                <div class="box-counter">
                                    <div class="counter">
                                        <span class="special">Offerta Speciale</span>
                                        <div class="counter-content">
                                            <p>Approfitta delle offerte speciali</p>
                                        </div><!-- /.counter-content -->
                                    </div><!-- /.counter -->
                                    <div class="product-item">
                                        <div class="imagebox style3">
                                            <div class="box-image save">
                                                <a href="#" title="">
                                                    <img src="{{ config('httpmedia'). 'ecommerce/prodotti/' . basename($product->meta('imghighlight')) }}" style="max-height: 200px" alt="">
                                                </a>
                                                <span>Risparmia {{$product->price-$product->price_offer}} &euro;</span>
                                            </div><!-- /.box-image -->
                                            <div class="box-content">
                                                <div class="product-name">
                                                    <a href="#" title="">{{$product->title}}</a>
                                                </div>
                                                <ul class="product-info">
                                                    {!! $product->description !!}
                                                </ul>
                                                <div class="price">
                                                    @if(($product->offer==1) && ($product->price_offert>0))
                                                        <span class="sale">{{$product->price_offer}} &euro;</span>
                                                        <span class="regular">{{$product->price}} &euro;</span>
                                                    @else
                                                        <span class="sale">{{$product->price}} &euro;</span>
                                                    @endif
                                                </div>
                                            </div><!-- /.box-content -->
                                            <div class="box-bottom">
                                                <div class="btn-add-cart">
                                                    <a href="{{ path_for('product', ['permalink' => $product->permalink]) }}" title="">
                                                        <img src="{{asset('assets/images/')}}icons/add-cart.png" alt="">Vai al prodotto
                                                    </a>
                                                </div>
                                                <div class="compare-wishlist">
                                                    <a href="#" class="compare" title="">
                                                        <img src="{{asset('assets/images/')}}icons/compare.png" alt="">Compare
                                                    </a>
                                                    <a href="#" class="wishlist" title="">
                                                        <img src="{{asset('assets/images/')}}icons/wishlist.png" alt="">Wishlist
                                                    </a>
                                                </div>
                                            </div><!-- /.box-bottom -->
                                        </div><!-- /.imagbox style3 -->
                                    </div><!-- /.product-item -->
                                </div><!-- /.box-counter -->
                            @endforeach
                        </div><!-- /.owl-carousel-2 -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-imagebox style3 -->
    @endif



    <section class="flat-imagebox">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-tab">
                        <ul class="tab-list">
                            @if(count($category) > 0)
                                @foreach($category as $cat)
                                    <li>Novit&agrave; {{$cat->title}}</li>
                                @endforeach
                            @endif
                        </ul>
                    </div><!-- /.product-tab -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
            <div class="box-product">
                @if(count($category) > 0)
                    @foreach($category as $cat)
                        <div class="row">
                            @if(count($productNovelty[$cat->id])>0)

                                @php
                                    $posizione= 0;
                                @endphp
                                @foreach($productNovelty[$cat->id] as $product)
                                    @if($posizione%2==0)
                                        <div class="col-sm-6 col-lg-3">
                                            @endif
                                            <div class="product-box">

                                                <div class="imagebox">
                                                    @if($product->meta('imghighlight'))
                                                    <ul class="box-image">
                                                            <li>
                                                                <a href="{{ path_for('product', ['permalink' => $product->permalink]) }}" title="">
                                                                    <img src="{{ config('httpmedia'). 'ecommerce/prodotti/' . basename($product->meta('imghighlight')) }}" alt="">
                                                                </a>
                                                            </li>
                                                    </ul><!-- /.box-image -->
                                                    @endif
                                                    <div class="box-content">
                                                        <div class="cat-name">
                                                            <a href="{{ path_for('product', ['permalink' => $product->permalink]) }}" title="">{{$product->category->title}}</a>
                                                        </div>
                                                        <div class="product-name">
                                                            <a href="{{ path_for('product', ['permalink' => $product->permalink]) }}" title="">{{$product->title}}</a>
                                                        </div>
                                                        <div class="price">
                                                            @if(($product->offer==1) && ($product->price_offert>0))
                                                                <span class="sale">{{$product->price_offer}} &euro;</span>
                                                                <span class="regular">{{$product->price}} &euro;</span>
                                                            @else
                                                                <span class="sale">{{$product->price}} &euro;</span>
                                                            @endif
                                                        </div>
                                                    </div><!-- /.box-content -->
                                                    <div class="box-bottom">
                                                        <div class="btn-add-cart">
                                                            <a href="{{ path_for('product', ['permalink' => $product->permalink]) }}" title="">
                                                                <img src="{{asset('assets/images/')}}icons/add-cart.png" alt="">Vai al prodotto
                                                            </a>
                                                        </div>
                                                        <div class="compare-wishlist">
                                                            <a href="#" class="compare" title="">
                                                                <img src="{{asset('assets/images/')}}icons/compare.png" alt="">Compare
                                                            </a>
                                                            <a href="#" class="wishlist" title="">
                                                                <img src="{{asset('assets/images/')}}icons/wishlist.png" alt="">Wishlist
                                                            </a>
                                                        </div>
                                                    </div><!-- /.box-bottom -->
                                                </div><!-- /.imagebox -->
                                            </div><!-- /.product-box -->
                                            @if($posizione%2==1)
                                                </div>
                                            @endif
                                    @php
                                        $posizione++;
                                    @endphp
                                @endforeach
                                @if($posizione>0 && $posizione%2==1)
                                </div>
                                @endif
                                @php
                                    $posizione= 0;
                                @endphp

                    @endif
            </div>
            @endforeach
            @endif
        </div><!-- /.box-product -->
        </div><!-- /.container -->
    </section><!-- /.flat-imagebox -->







    <section class="flat-brand">
        <div class="container">
            <div class="flat-row-title">
                <h3>I nostri marchi </h3>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(count($brands) > 0)

                        <ul class="owl-carousel-5">
                        @foreach($brands as $brand)
                                <li>
                                    <img src="{{ config('httpmedia'). 'ecommerce/brand/' . basename($brand->meta('imghighlight')) }}" alt="">
                                </li>
                        @endforeach
                        </ul><!-- /.owl-carousel-5 -->
                    @endif
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-brand -->

    <div class="divider30"></div>
@endsection