<?php
//Controller Namespace
const _CONTROLLERS_F = "\\Plugins\\Frontend\\Controllers\\";

router()->group(['middleware' => ['web', '_offline']], function () {
  //frontpage
  router()->get('/', [
    'as' => 'frontpage',
    'uses' => _CONTROLLERS_F . "Index@frontpage"
  ]);

  //frontpage
  router()->get('/ajax-search', [
    'as' => 'ajax-search',
    'uses' => _CONTROLLERS_F . "Index@ajaxSearch"
  ]);

  router()->get('/facebook-feed', [ 'as' => 'facebook-feed', 'uses' => _CONTROLLERS_F . 'Index@facebookFeed' ]);

  //ricerca
  router()->get('/search', [
    'as' => 'search',
    'uses' => _CONTROLLERS_F . "Index@search"
  ]);

  router()->get('/ricerca', [
    'as' => 'shop-page-search',
    'uses' => _CONTROLLERS_F . "Index@shopListSearch"
  ]);

  //page contatti
  /*  router()->post( '/contatti/', [
        'as'   => 'mailer',
        'uses' => _CONTROLLERS_F . "Index@contattiPage"
    ] );
*/

  // shop list
  router()->match(['GET', 'POST'], '/shop', [
    'as' => 'shop-page',
    'uses' => _CONTROLLERS_F . "Index@shopList"
  ]);

  // Logout
  router()->match(['GET', 'POST'], '/logout', [
    'as' => 'logout',
    'uses' => _CONTROLLERS_F . "Index@logout"
  ]);

  //no access
  router()->match(['GET', 'POST'], '/denied', [
    'as' => 'noaccess',
    'uses' => _CONTROLLERS_F . "Index@noaccess"
  ]);

  // shop list
  router()->match(['GET', 'POST'], '/offerte', [
    'as' => 'shop-page-offerte',
    'uses' => _CONTROLLERS_F . "Index@shopOffer"
  ]);

  // Pagina singola
  router()->match(['GET', 'POST'], '/{id}', [
    'as' => 'single-page',
    'uses' => _CONTROLLERS_F . "Index@single"
  ]);

  // pagina singola
  router()->get('/{permalink}', [
    'as' => 'page',
    'uses' => _CONTROLLERS_F . "Index@single"
  ]);


  //mail contatti
  router()->post('/contact/mailc', [
    'as' => 'mailer',
    'uses' => _CONTROLLERS_F . "Index@mailc"
  ]);


  router()->match(['GET', 'POST'], '/customer/registration-page/recovery-password/{id_customer}/{password}', [
    'as' => 'recovery-password',
    'uses' => _CONTROLLERS_F . "Index@recoveryPasswordPage"
  ]);


  // Login
  router()->match(['GET', 'POST'], '/customer/login', [
    'as' => 'login',
    'uses' => _CONTROLLERS_F . "Index@login"
  ]);

  router()->match(['GET', 'POST'], '/registration-page/recuperapass', [
    'as' => 'recuperapass',
    'uses' => _CONTROLLERS_F . "Index@recuperaPassword"
  ]);

  router()->match(['GET', 'POST'], '/customer/recuperapass', [
    'as' => 'save-password',
    'uses' => _CONTROLLERS_F . "Index@savePassword"
  ]);

  // Register
  router()->match(['GET', 'POST'], '/customer/register', [
    'as' => 'register',
    'uses' => _CONTROLLERS_F . "Index@register"
  ]);


  // send contact
  router()->match(['GET', 'POST'], '/send/contact', [
    'as' => 'sendcontact',
    'uses' => _CONTROLLERS_F . "Index@sendContactForm"
  ]);


  // Login Page
  router()->match(['GET', 'POST'], '/customer/access/{error?}', [
    'as' => 'loginp',
    'uses' => _CONTROLLERS_F . "Index@loginPage"
  ]);


  // Login Page
  router()->match(['GET', 'POST'], '/customer/registration-page/{error?}', [
    'as' => 'registrationp',
    'uses' => _CONTROLLERS_F . "Index@registrationPage"
  ]);



  //archivio
  router()->get('/categoria/{code_post_type}/{code_cat?}', [
    'as' => 'archive',
    'uses' => _CONTROLLERS_F . "Index@archive"
  ]);


  // prodotto singolo
  router()->get('/{permalink}', [
    'as' => 'product',
    'uses' => _CONTROLLERS_F . "Index@product"
  ]);


  // prodotto singolo
  router()->match(['GET', 'POST'], '/product/carrello/save', [
    'as' => 'save-carrello',
    'uses' => _CONTROLLERS_F . "Index@savecarrello"
  ]);

  // checkout
  router()->match(['GET', 'POST'], '/section/checkout', [
    'as' => 'checkout',
    'uses' => _CONTROLLERS_F . "Index@checkout"
  ]);

  // proceed-checkout
  router()->match(['GET', 'POST'], '/proceed/checkout', [
    'as' => 'proceed_checkout',
    'uses' => _CONTROLLERS_F . "Index@proceedCheckout"
  ]);

  // save-order
  router()->match(['GET', 'POST'], '/save/order', [
    'as' => 'save_order',
    'uses' => _CONTROLLERS_F . "Index@saveOrder"
  ]);

    // ipn-paypal
    router()->match(['GET', 'POST'], '/paypal/ipn', [
        'as' => 'pagamento-paypal',
        'uses' => _CONTROLLERS_F . "Index@pagamentoPaypal"
    ]);


  /*AREA UTENTE*/
  // prodotto singolo
  router()->match(['GET', 'POST'], '/user/area', [
    'as' => 'area-user',
    'uses' => _CONTROLLERS_F . "Index@userArea"
  ]);


  router()->match(['GET', 'POST'], '/user/impostazioni', [
    'as' => 'area-user-impostazioni',
    'uses' => _CONTROLLERS_F . "Index@userData"
  ]);

  router()->match(['GET', 'POST'], '/user/savedata', [
    'as' => 'user-save',
    'uses' => _CONTROLLERS_F . "Index@userDataSave"
  ]);

  router()->match(['GET', 'POST'], '/user/order', [
    'as' => 'user-save-order',
    'uses' => _CONTROLLERS_F . "Index@userOrder"
  ]);

    // wishlist
    router()->match(['GET', 'POST'], '/user/wishlist', [
        'as' => 'wishlist',
        'uses' => _CONTROLLERS_F . "Index@wishlist"
    ]);


  router()->match(['GET', 'POST'], '/user/order/info', [
    'as' => 'user-info-order',
    'uses' => _CONTROLLERS_F . "Index@userOrderInfo"
  ]);


  router()->match(['GET', 'POST'], '/register-mail/newsletter', [
    'as' => 'subscribe-mailchimp',
    'uses' => _CONTROLLERS_F . "Index@registerMailChimp"
  ]);


  /*FINE AREA UTENTE*/


  router()->match(['GET', 'POST'], '/auth/facebook', [
    'as' => 'auth-facebook',
    'uses' => _CONTROLLERS_F . "Index@authFacebook"
  ]);

    // shop list categoria
    router()->match(['GET', 'POST'], '/{category}', [
        'as' => 'shop-page-cat',
        'uses' => _CONTROLLERS_F . "Index@shopList"
    ])->where('category','^[a-zA-Z0-9-_\/]+$');

  // nel caso ho bisogno di passare gli id dei post faccio un redirect alla regola col permalink
  router()->get('/post/{id}', function ($id) {
    $post = \Plugins\CMS\Models\Post::find($id);
    if ($post->permalink !== '/') {
      return redirector()->route('page', ['permalink' => $post->permalink], '301');
    } else {
      return redirector()->route('frontpage', [], '301');
    }
  }
  )->where('id', '[0-9]+');




});